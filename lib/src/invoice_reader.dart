part of invoice_reader;

const InvoiceReaderChannel = const MethodChannel('invoice_reader');

class InvoiceReader {
  static final TextDetector textDetector = TextDetector._();
  static final PathProvider pathProvider = PathProvider._();
  static final DocumentDetector documentDetector = DocumentDetector._();

  static Future<Invoice> process(
    Uint8List bytes, {
    required InvoiceParser parser,
    double ratioPriceRange = 0.3,
    double minAccuracy = 0.95,
    bool enhance = false,
  }) async {
    Uint8List document = bytes;

    final detectResult = await documentDetector.detectDocument(bytes);
    if (detectResult.points.isNotEmpty) {
      document = detectResult.document;
    }

    if (enhance) {
      final enhancedPicture = await documentDetector.enhancePicture(document);
      document = enhancedPicture;
    }

    final recognisedText = await textDetector.processImage(document);

    final dictionary = InvoiceDictionary._(
      parser.type,
      minAccuracy: minAccuracy,
      ratioPriceRange: ratioPriceRange,
    );

    final parsedInvoice = await compute(parser.parse, recognisedText);
    final prepraredInvoiceItems = <InvoiceItem>[];

    for (int i = 0; i < parsedInvoice.items.length; i++) {
      var item = parsedInvoice.items[i].copyWith();

      int? quantity = item.quantity;
      double? price = item.price;
      double? totalPrice = item.totalPrice;
      if (item.weight == null &&
          quantity == null &&
          price != null &&
          totalPrice != null) {
        quantity = totalPrice ~/ price;
        if ((totalPrice / price) > quantity) {
          prepraredInvoiceItems.add(item);
          continue;
        }
        item = item.copyWith(quantity: quantity);
      }

      if (item.name == null) {
        prepraredInvoiceItems.add(item);
        continue;
      }

      final dictionaryMatches = await dictionary.search(item);
      if (dictionaryMatches.isEmpty) {
        prepraredInvoiceItems.add(item);
        continue;
      }

      prepraredInvoiceItems.add(item.copyWith(
        extra: <String, dynamic>{
          'productCode': dictionaryMatches.first.entry.productCode,
          'productName': dictionaryMatches.first.entry.productName,
        },
      ));
    }

    return parsedInvoice.copyWith(items: prepraredInvoiceItems);
  }

  static Future<bool> synchronize() =>
      InvoiceDictionaryManager.instance.synchronize();
}

part of invoice_reader;

class Invoice {
  final InvoiceType type;
  final String? number;
  final DateTime? date;
  final List<InvoiceItem> items;

  Invoice({required this.type, this.number, this.date, this.items = const []});

  Invoice copyWith({
    InvoiceType? type,
    String? number,
    DateTime? date,
    List<InvoiceItem>? items,
  }) {
    return Invoice(
      type: type ?? this.type,
      number: number ?? this.number,
      date: date ?? this.date,
      items: items ?? this.items,
    );
  }
}

enum InvoiceType {
  Aeon,
  BigC,
  Coopmart,
  CoopXtra,
  Emart,
  Fuji,
  Intimex,
  Lotte,
  Mega,
  Satra,
  Vinmart,
  Go,
}

class InvoiceItem {
  final String? code;
  final String? name;
  final double? originalPrice;
  final double? totalPrice;
  final double? price;
  final int? tax;
  final double? weight;
  final int? quantity;
  final Map<String, dynamic>? extra;

  const InvoiceItem({
    this.code,
    this.name,
    this.originalPrice,
    this.totalPrice,
    this.price,
    this.tax,
    this.weight,
    this.quantity,
    this.extra,
  });

  InvoiceItem copyWith({
    String? code,
    String? name,
    double? originalPrice,
    double? totalPrice,
    double? price,
    int? tax,
    double? weight,
    int? quantity,
    Map<String, dynamic>? extra,
  }) {
    return InvoiceItem(
      code: code ?? this.code,
      name: name ?? this.name,
      originalPrice: originalPrice ?? this.originalPrice,
      totalPrice: totalPrice ?? this.totalPrice,
      price: price ?? this.price,
      tax: tax ?? this.tax,
      weight: weight ?? this.weight,
      quantity: quantity ?? this.quantity,
      extra: extra ?? this.extra,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
      'extra': extra,
    };
  }
}

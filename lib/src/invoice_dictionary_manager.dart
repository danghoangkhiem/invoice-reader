part of invoice_reader;

class InvoiceDictionaryManager {
  static final InvoiceDictionaryManager instance = InvoiceDictionaryManager._();

  bool get isReady => _completer.isCompleted;

  InvoiceDictionaryManager._() {
    _initHive();
  }

  final Completer<Box<InvoiceDictionaryEntry>> _completer = Completer();
  final HttpClient _httpClient = HttpClient(context: _securityContext);
  final String _baseUrl = 'https://sp-ocr-dictionary.iqc.xyz/api/v1';
  final String _boxName = 'dictionary_1_0_0';

  bool _isSyncing = false;

  Future<bool> synchronize() async {
    final box = await _waitHiveInitialized();
    if (_isSyncing) {
      return false;
    }

    _isSyncing = true;

    try {
      final poolData = <InvoiceDictionaryEntry>[];
      _InvoiceDictionaryResponseListData data;
      int index = 1;

      do {
        data = await _fetchData(index++);
        poolData.addAll(data.toList());
      } while (data.isNotEmpty);

      await box.clear();
      await box.addAll(poolData);
    } finally {
      _isSyncing = false;
    }

    return true;
  }

  Future<List<InvoiceDictionaryEntry>> get(InvoiceType type) async {
    final box = await _waitHiveInitialized();
    if (box.isEmpty) {
      return [];
    }
    return box.values
        .where((e) => type == e.invoiceType)
        .toList(growable: false);
  }

  Future<_InvoiceDictionaryResponseListData> _fetchData(int index) async {
    final endpoint = Uri.parse(_baseUrl + '/dictionary?p=$index');
    final req = await _httpClient.getUrl(endpoint);
    final res = await req.close();

    if (res.statusCode != 200) {
      throw HttpException('Invalid response from server');
    }

    final resBody = Completer<dynamic>();
    res
        .transform(convert.utf8.decoder)
        .transform(convert.json.decoder)
        .listen(resBody.complete);

    final body = await resBody.future;

    return _InvoiceDictionaryResponseListData.fromJson(List.from(body));
  }

  Future<void> _initHive() async {
    final appPath =
        await InvoiceReader.pathProvider.getApplicationDocumentsPath();

    if (appPath != null) {
      Hive.init(appPath);
    } else {
      Hive.init(Directory.systemTemp.path);
    }

    Hive.registerAdapter(_InvoiceTypeAdapter());
    Hive.registerAdapter(_InvoiceDictionaryEntryAdapter());
    Hive.registerAdapter(_InvoiceDictionaryDefinitionAdapter());

    final box = await Hive.openBox<InvoiceDictionaryEntry>(_boxName);

    _completer.complete(box);
  }

  Future<Box<InvoiceDictionaryEntry>> _waitHiveInitialized() async {
    return await _completer.future;
  }

  static SecurityContext get _securityContext =>
      SecurityContext(withTrustedRoots: true)
        ..usePrivateKeyBytes(_key.codeUnits)
        ..useCertificateChainBytes(_pem.codeUnits);

  static String get _pem {
    return """
-----BEGIN CERTIFICATE-----
MIIEFTCCAv2gAwIBAgIUDBqJ1zdb5S1B5IEphvPBsdIjnY0wDQYJKoZIhvcNAQEL
BQAwgagxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMRYwFAYDVQQH
Ew1TYW4gRnJhbmNpc2NvMRkwFwYDVQQKExBDbG91ZGZsYXJlLCBJbmMuMRswGQYD
VQQLExJ3d3cuY2xvdWRmbGFyZS5jb20xNDAyBgNVBAMTK01hbmFnZWQgQ0EgYWFm
Yjg2YzE1ZDcyMGM1NDI3YzNkMTdkODk1NTBhZDIwHhcNMjEwMTA3MDMwNzAwWhcN
MzEwMTA1MDMwNzAwWjAiMQswCQYDVQQGEwJVUzETMBEGA1UEAxMKQ2xvdWRmbGFy
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJOOtfwyOCTjTHpA8QV8
ojKy2LpTWN75crzkqyXPPMvaqEg1js6zBXoJaBTsY382pmNseaBdHADja9vLTQjv
t2BFb1tRs8wKMlu55KxsPKUok7CL5OeSd3aR9mbfM2xlpt4AhckDs3Y1MvYmfjcR
qaeg+EuqDqExB18hKFlh4B0UA1u3qCaEPIOOg5L6w2Crb8Rax6WvzICYst2Qdhfw
3LSasVj9eaK0SC+jS+q5FX1ye56U76v4xkUOyK/AGYJgX5Wp30CYjhzsycKYZ2LP
6R8deSOOuoUhAWICr+W44e3hEiVtsdTDMbBUjlP8OgRdN/FFkvJMvaOlLs+PGs0p
cA0CAwEAAaOBuzCBuDATBgNVHSUEDDAKBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAA
MB0GA1UdDgQWBBQv3PrF1xnOJLeExmbA3KxE7jPSpTAfBgNVHSMEGDAWgBTKMMoz
B2L31uGDMDWjvLmBeY95lDBTBgNVHR8ETDBKMEigRqBEhkJodHRwOi8vY3JsLmNs
b3VkZmxhcmUuY29tLzYzYmQ5ZWYzLWNlY2ItNGQxMi1hMjk1LTdjMDY5NzhkOWNi
Ni5jcmwwDQYJKoZIhvcNAQELBQADggEBALg0KB5s/F9WEMJ5cvHVcfJaV9Vm2Edq
2AUkXRqPhtOj4+aZ3l/TnZebXcnwe5XB2Koru8Ki67pf6jT+Qpjg12Pa4KFHu0H+
zEwNRBwhwrgf6yzOa0rbtyemrKE4uaZ/5Hc2Gc5pZ6QNn+pfewKwUlalz7kQVxp0
IksPUaqtkOEQrtI/lxf85udGaiwpphC1UG79NqIlcnG5XpujO5TCgvauVEvvEB6l
SGCKWDLGQsW1NkjIoN9ObkYlSrsc2jAMhOpf/VRvQf7upc8LzvOqrtAsS9mqqZ0n
vILx8Uy2OdQ30vXgi4NkWh+9kcHLQCa8OZauongk8MkAsPkEid1RPG8=
-----END CERTIFICATE-----
""";
  }

  static String get _key {
    return """
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCTjrX8Mjgk40x6
QPEFfKIysti6U1je+XK85KslzzzL2qhINY7OswV6CWgU7GN/NqZjbHmgXRwA42vb
y00I77dgRW9bUbPMCjJbueSsbDylKJOwi+Tnknd2kfZm3zNsZabeAIXJA7N2NTL2
Jn43EamnoPhLqg6hMQdfIShZYeAdFANbt6gmhDyDjoOS+sNgq2/EWselr8yAmLLd
kHYX8Ny0mrFY/XmitEgvo0vquRV9cnuelO+r+MZFDsivwBmCYF+Vqd9AmI4c7MnC
mGdiz+kfHXkjjrqFIQFiAq/luOHt4RIlbbHUwzGwVI5T/DoEXTfxRZLyTL2jpS7P
jxrNKXANAgMBAAECggEACFU118EqRIMFjZ3fPpUUP9KzxInyad0iaqJrO95hK430
lOdZYjjf5U1ksFHNKKD4coLst26qpy4wqB/qAMgwzeH98OVDRxGIWH+99zLBlf4d
oQm1apJt5qdFh/ew2r0+2CilZX2tA87rStVPqWJ49zty6ir3Woj/RpMDyVBqrJmf
w4HUFowwhhvjquy/jVR/bLyn8NH+e30qUx+jxzKwRZLdgfRN+ymit8j9aGs8ADsY
wWjKZJniU7l4K3Y+bey2S00fqbWccMS3HAR4UQAiaP4sbSWpGvPu7TDUwp358FJ8
y0jPOYMH6f1kPAMKdYp3zSuwq/XUwzR2qFh0OV74TQKBgQDJmh6t3GnIEuelLXHN
CAUduUZxF/k+tUOQtyHs5tVgUZ3VcOK+vVS7Io8V2sXjLJVTMzwMi7B/CTSZQyvL
vcxr1VaXSx7LIbewBGWaT/QO0j++qeSkghuRwubhF1xDvfkUwDdt57hkfAOZR4Bq
sbkvIMEzSxsuYDkONcHOCpFOUwKBgQC7X2dXXe+iJB7lHnfYHQVyZhortD7eNMXT
NOQQOU5umoWsMQXqCbbaN1zB90fmv+AzzCXsF1cV8Ts9H+joJQ+dJh2JwGck+Djp
04g0GuuqFGH0EnS88mrq3/AqJjn+hV8X50epPQ4F7DgtvZw33k0QPaJV2uX5ttov
5pLPtkS8HwKBgF/aX2vauJDgLDNlAA60GXqgdNWgyGkAVjegWwkAV8OMCrdJDQYT
jw3KyK1jb2Mr93OvkIWGhUxhXBjizxf4DhHXRWx50xJR3bxa2tQb37aMULKepI9z
LRiIyq2LJdsvoBY1riUbX5inPi7KyEvyZFH8COASfcH3DimrdZI5hAzxAoGBAK/n
swbsY0ofCvoijnRPWou5mxwojVTiyk2sO+eXRTDE1HWBWIINOKLIksZuNgxFWmCG
aZupldI9tFWAHZHDKVNpMxXdkXe9qQCr3BS+qTv1Ari2ku3Hz+RM9BU96fgukcXJ
FRenAAFDcMJQXdZ9Vm/GzR448OpsZY3Os0lZXlIVAoGAeRAOz3odePNxa8tEagcf
DeU5AT+/bVKu1oNu9kfM1LtZHWhv07BEkx2xcnU0V9jX+GQah09kU+t88gdiEFPW
fCzyfq6qn6b36AulkcsBmBp0HCmguQXqko+skyMPfF8710fMsI+VMq0Bphu5ZOdu
OdW2pMrp0TPekTImkA5QBns=
-----END PRIVATE KEY-----
""";
  }
}

class _InvoiceDictionaryResponseListData
    extends ListMixin<InvoiceDictionaryEntry> {
  final List<InvoiceDictionaryEntry> _;

  @override
  int get length => _.length;

  @override
  InvoiceDictionaryEntry operator [](int index) {
    return _[index];
  }

  @override
  void operator []=(int index, InvoiceDictionaryEntry value) {
    _[index] = value;
  }

  @override
  set length(int newLength) {
    _.length = newLength;
  }

  _InvoiceDictionaryResponseListData.fromJson(List<dynamic> list) : _ = [] {
    for (int i = 0; i < list.length; i++) {
      final item = Map<String, dynamic>.of(list[i]);
      final typeIndex = (item['t'] as int) - 1;
      _.add(InvoiceDictionaryEntry._(
        invoiceType: InvoiceType.values[typeIndex],
        productCode: item['c'],
        productName: item['n'],
        definition: InvoiceDictionaryDefinition._(
          item['_n_'],
          item['_c_'],
          double.tryParse(item['_p_'].toString()),
        ),
      ));
    }
  }
}

// DO NOT change ordinal
enum _AdapterId {
  InvoiceType,
  InvoiceDictionaryEntry,
  InvoiceDictionaryDefinition
}

class _InvoiceTypeAdapter extends TypeAdapter<InvoiceType> {
  @override
  int get typeId => _AdapterId.InvoiceType.index;

  @override
  InvoiceType read(BinaryReader reader) {
    return InvoiceType.values[reader.readInt()];
  }

  @override
  void write(BinaryWriter writer, InvoiceType obj) {
    writer.writeInt(obj.index);
  }
}

class _InvoiceDictionaryEntryAdapter
    extends TypeAdapter<InvoiceDictionaryEntry> {
  @override
  int get typeId => _AdapterId.InvoiceDictionaryEntry.index;

  @override
  InvoiceDictionaryEntry read(BinaryReader reader) {
    return InvoiceDictionaryEntry._(
      invoiceType: reader.read(),
      productCode: reader.read(),
      productName: reader.read(),
      definition: reader.read(),
    );
  }

  @override
  void write(BinaryWriter writer, InvoiceDictionaryEntry obj) {
    writer.write(obj.invoiceType);
    writer.write(obj.productCode);
    writer.write(obj.productName);
    writer.write(obj.definition);
  }
}

class _InvoiceDictionaryDefinitionAdapter
    extends TypeAdapter<InvoiceDictionaryDefinition> {
  @override
  int get typeId => _AdapterId.InvoiceDictionaryDefinition.index;

  @override
  InvoiceDictionaryDefinition read(BinaryReader reader) {
    return InvoiceDictionaryDefinition._(
      reader.read(),
      reader.read(),
      reader.read(),
    );
  }

  @override
  void write(BinaryWriter writer, InvoiceDictionaryDefinition obj) {
    writer.write(obj.name);
    writer.write(obj.code);
    writer.write(obj.approximatePrice);
  }
}

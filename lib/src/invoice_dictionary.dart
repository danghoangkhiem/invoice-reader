part of invoice_reader;

class InvoiceDictionary {
  final InvoiceType type;
  final double minAccuracy;
  final double ratioPriceRange;

  InvoiceDictionary._(
    this.type, {
    this.minAccuracy = 0.95,
    this.ratioPriceRange = 0.3,
  });

  Future<List<InvoiceDictionaryMatch>> search(InvoiceItem item) async {
    final dictionaryEntries = await getDictionaryEntries();
    final founds = <InvoiceDictionaryMatch>[];

    double maxAccuracy = minAccuracy;

    for (int i = 0; i < dictionaryEntries.length; i++) {
      final entry = dictionaryEntries[i];

      final accuracy = compareTwoStrings(entry.definition.name, item.name);
      if (accuracy <= minAccuracy) {
        continue;
      }

      bool isMaxAccuracy = accuracy > maxAccuracy;
      if (isMaxAccuracy) {
        maxAccuracy = accuracy;
      }

      final found = InvoiceDictionaryMatch._(entry: entry, accuracy: accuracy);
      final acceptances = <bool>[true];

      double? definitionApproximatePrice = entry.definition.approximatePrice;
      if (definitionApproximatePrice != null) {
        final diff = (definitionApproximatePrice - (item.price ?? 0.0)).abs();
        final ratioDefinitionApproximatePrice =
            definitionApproximatePrice * ratioPriceRange;
        acceptances.add(diff < ratioDefinitionApproximatePrice);
      }

      String? definitionCode = entry.definition.code;
      if (definitionCode != null) {
        acceptances.add(definitionCode == item.code);
      }

      if (acceptances.every((e) => e)) {
        if (isMaxAccuracy) {
          founds.insert(0, found);
        } else {
          founds.add(found);
        }
      }
    }

    return founds;
  }

  @visibleForTesting
  Future<List<InvoiceDictionaryEntry>> getDictionaryEntries() {
    return InvoiceDictionaryManager.instance.get(type);
  }

  @visibleForTesting
  double compareTwoStrings(String? first, String? second) {
    // if both are null
    if (first == null && second == null) {
      return 1;
    }
    // as both are not null if one of them is null then return 0
    if (first == null || second == null) {
      return 0;
    }

    first =
        first.replaceAll(RegExp(r'\s+\b|\b\s'), ''); // remove all whitespace
    second =
        second.replaceAll(RegExp(r'\s+\b|\b\s'), ''); // remove all whitespace

    // if both are empty strings
    if (first.isEmpty && second.isEmpty) {
      return 1;
    }
    // if only one is empty string
    if (first.isEmpty || second.isEmpty) {
      return 0;
    }
    // identical
    if (first == second) {
      return 1;
    }
    // both are 1-letter strings
    if (first.length == 1 && second.length == 1) {
      return 0;
    }
    // if either is a 1-letter string
    if (first.length < 2 || second.length < 2) {
      return 0;
    }

    final firstBigrams = <String, int>{};
    for (var i = 0; i < first.length - 1; i++) {
      final bigram = first.substring(i, i + 2);
      final count =
          firstBigrams.containsKey(bigram) ? firstBigrams[bigram]! + 1 : 1;
      firstBigrams[bigram] = count;
    }

    var intersectionSize = 0;
    for (var i = 0; i < second.length - 1; i++) {
      final bigram = second.substring(i, i + 2);
      final count =
          firstBigrams.containsKey(bigram) ? firstBigrams[bigram]! : 0;

      if (count > 0) {
        firstBigrams[bigram] = count - 1;
        intersectionSize++;
      }
    }

    return (2.0 * intersectionSize) / (first.length + second.length - 2);
  }
}

class InvoiceDictionaryMatch {
  final InvoiceDictionaryEntry entry;
  final double accuracy;

  InvoiceDictionaryMatch._({
    required this.entry,
    required this.accuracy,
  });
}

class InvoiceDictionaryEntry extends HiveObject {
  final InvoiceType invoiceType;
  final String productCode;
  final String productName;
  final InvoiceDictionaryDefinition definition;

  InvoiceDictionaryEntry._({
    required this.invoiceType,
    required this.productCode,
    required this.productName,
    required this.definition,
  });
}

class InvoiceDictionaryDefinition extends HiveObject {
  final String? name;
  final String? code;
  final double? approximatePrice;

  InvoiceDictionaryDefinition._(this.name, [this.code, this.approximatePrice]);
}

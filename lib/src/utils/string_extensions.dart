class ReplacePattern {
  final Pattern from;
  final String to;
  final bool isFirst;

  ReplacePattern({
    required this.from,
    required this.to,
    this.isFirst = false,
  });
}

extension StringReplacePatterns on String {
  String replacePattern(ReplacePattern pattern) {
    String string = this;
    if (pattern.isFirst) {
      string = string.replaceFirst(pattern.from, pattern.to);
    } else {
      string = string.replaceAll(pattern.from, pattern.to);
    }
    return string;
  }

  String replacePatterns(List<ReplacePattern> patterns) {
    String string = this;
    for (final pattern in patterns) {
      if (pattern.isFirst) {
        string = string.replaceFirst(pattern.from, pattern.to);
      } else {
        string = string.replaceAll(pattern.from, pattern.to);
      }
    }
    return string;
  }
}
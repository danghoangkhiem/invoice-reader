import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class LotteInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.Lotte;

  final _invoiceDate = RegExp(
      r'((?<Y>\d{4})\-(?<M>\d{1,2})\-(?<D>\d{1,2})\s(?<h>\d{2})\:(?<m>\d{2}))\b');
  final _invoiceNumberRegex =
      RegExp(r'([\dODUIZzBAqSsGT/Rg]{4}\-[\dODUIZzBAqSsGT/Rg]{4})\b');
  final _itemRegex = RegExp(r'^((?:\s*[\dODUIZzBAqSsGT/Rg]){3})\s+([^,:]+?)$');
  final _itemCodeAndPriceAndQuantityRegex = RegExp(
      r'((?:\s*[\d]){13})\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*)\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*)\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*)\b');

  final _dotOrCommaRegex = RegExp(r'\.|,');

  final _dotToNon = ReplacePattern(from: '.', to: '');
  final _doubleDotToDot = ReplacePattern(from: '..', to: '.');
  final _dotAfterSpaceToNon = ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterDotToNon =
      ReplacePattern(from: RegExp(r'(?<=\.)\s+'), to: '');

  final _commaToNon = ReplacePattern(from: ',', to: '');
  final _doubleCommaToComma = ReplacePattern(from: ',,', to: ',');
  final _commaAfterSpaceToNon =
      ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterCommaToNon =
      ReplacePattern(from: RegExp(r'(?<=,)\s+'), to: '');

  final _spaceBetweenNumberToNon =
      ReplacePattern(from: RegExp(r'(?<=\d)\s+(?=\d)'), to: '');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawLotteInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks = prepareTextBlockAccordingLine(recognisedText);
    final rawInvoiceItems = <RawLotteInvoiceItem>[];

    DateTime? invoiceDate;
    String? invoiceNumber;
    RawLotteInvoiceItem? rawInvoiceItem;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }

      final expanedText = textBlock.text;
      print(expanedText);

      if (invoiceDate == null) {
        final invoiceDateMatch = _invoiceDate.firstMatch(expanedText);
        if (invoiceDateMatch != null) {
          invoiceDate = DateTime(
            int.parse(invoiceDateMatch.namedGroup('Y').toString()),
            int.parse(invoiceDateMatch.namedGroup('M').toString()),
            int.parse(invoiceDateMatch.namedGroup('D').toString()),
            int.parse(invoiceDateMatch.namedGroup('h').toString()),
            int.parse(invoiceDateMatch.namedGroup('m').toString()),
          );
        }
      }

      if (invoiceNumber == null && rawInvoiceItems.isEmpty) {
        final invoiceNumberMatch =
            _invoiceNumberRegex.firstMatch(textBlock.text);
        if (invoiceNumberMatch != null) {
          invoiceNumber =
              invoiceNumberMatch.group(1).toString().trim().replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
        }
      }

      final itemMatch = _itemRegex.firstMatch(expanedText);
      if (itemMatch != null) {
        rawInvoiceItem = RawLotteInvoiceItem._(
          name: itemMatch.group(2).toString().trim(),
        );
      } else if (rawInvoiceItem != null) {
        final itemExtraMatch = _itemCodeAndPriceAndQuantityRegex.firstMatch(
          expanedText.replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
            _spaceAfterCommaToNon,
            _commaAfterSpaceToNon,
            _doubleCommaToComma,
            _spaceAfterDotToNon,
            _dotAfterSpaceToNon,
            _doubleDotToDot,
          ]),
        );
        if (itemExtraMatch != null) {
          rawInvoiceItem.code = itemExtraMatch
              .group(1)
              .toString()
              .trim()
              .replacePatterns([_spaceBetweenNumberToNon]);
          rawInvoiceItem.price = itemExtraMatch
              .group(2)
              .toString()
              .trim()
              .replacePatterns(
                  [_dotToNon, _commaToNon, _spaceBetweenNumberToNon]);
          final matchedNumber = itemExtraMatch
              .group(3)
              .toString()
              .trim()
              .replacePatterns([_spaceBetweenNumberToNon]);
          if (matchedNumber.contains(_dotOrCommaRegex)) {
            rawInvoiceItem.weight =
                matchedNumber.replaceAll(_dotOrCommaRegex, '.');
          } else {
            rawInvoiceItem.quantity = matchedNumber;
          }
          rawInvoiceItem.totalPrice = itemExtraMatch
              .group(4)
              .toString()
              .trim()
              .replacePatterns(
                  [_dotToNon, _commaToNon, _spaceBetweenNumberToNon]);
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? originalPrice, totalPrice, price, weight;
      int? tax, quantity;
      var _originalPrice = e.originalPrice;
      if (_originalPrice != null) {
        originalPrice = double.tryParse(_originalPrice);
      }
      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _weight = e.weight;
      if (_weight != null) {
        weight = double.tryParse(_weight);
      }
      var _tax = e.tax;
      if (_tax != null) {
        tax = int.tryParse(_tax);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        quantity = int.tryParse(_quantity);
      }
      return InvoiceItem(
        code: e.code ?? '',
        name: e.name,
        originalPrice: originalPrice,
        totalPrice: totalPrice,
        price: price,
        tax: tax,
        weight: weight,
        quantity: quantity,
      );
    }).toList();

    return Invoice(
      type: type,
      date: invoiceDate,
      number: invoiceNumber,
      items: invoiceItems,
    );
  }
}

class RawLotteInvoiceItem {
  final String name;
  String? code;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondarTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;

  TextBlock? get secondary => _secondarTextBlock;

  TextBlock? get extra => _extraTextBlock;

  RawLotteInvoiceItem._({required this.name});

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondarTextBlock != null) {
      _secondarTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class BigcInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.BigC;

  final _invoiceNumberRegex =
      RegExp(r'(?<=et\:\s+?)([\dODUIZzBAqSsGT/Rg]{9})\b');
  final _itemRegex = RegExp(r'(.*?)([\d]{1,3}(?:[.,][\d]{3})*)(?=d)');
  final _priceAndQuantityRegex = RegExp(
      r'([\d]{1,3}(?:[.,][\d]{3})*)\s[xX]\s([\d]{1,3}(?:[.,][\d]{3})*)(?=d)');
  final _taxRegex = RegExp(r'^(\d)\s*(.)?$');
  final _unitWeightRegex = RegExp(r'(Kg|K9)$');
  final _spaceAroundDotRegex = RegExp(r'\s+\.\s+');
  final _spaceBetweenNumbersRegex = RegExp(r'(?<=\d)\s+(?=\d)');

  final _dotToNon = ReplacePattern(from: '.', to: '');
  final _commaToDot = ReplacePattern(from: ',', to: '.');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawBigCInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks = prepareTextBlockAccordingLine(recognisedText);
    final rawInvoiceItems = <RawBigCInvoiceItem>[];

    final avgLineHeight = textBlocks
        .expand((e) => e.lines)
        .fold<double>(0.0, (val, elm) => ((val + elm.rect.height) / 2));

    String? invoiceNumber;
    RawBigCInvoiceItem? rawInvoiceItem;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }
      final lineHeight = textBlock.lines
          .fold<double>(0.0, (val, elm) => ((val + elm.rect.height) / 2));
      if (lineHeight > (avgLineHeight * 1.5)) {
        continue;
      }

      print(textBlock.text);

      if (invoiceNumber == null) {
        final invoiceNumberMatch =
            _invoiceNumberRegex.firstMatch(textBlock.text);
        if (invoiceNumberMatch != null) {
          invoiceNumber =
              invoiceNumberMatch.group(1).toString().trim().replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
        }
      }

      final taxMatch = _taxRegex.firstMatch(textBlock.lines.first.text);
      if (taxMatch != null) {
        final expanedText = textBlock.lines
            .skip(1)
            .fold<String>("", (val, e) => "$val ${e.text}")
            .replaceAll(_spaceAroundDotRegex, '')
            .trim();
        final itemMatches = _itemRegex.allMatches(expanedText);
        if (itemMatches.isNotEmpty) {
          rawInvoiceItem = RawBigCInvoiceItem._(
            name: itemMatches.first.group(1).toString().trim(),
          );
          rawInvoiceItem.totalPrice = itemMatches.first
              .group(2)
              .toString()
              .replacePatterns([_commaToDot, _dotToNon]);
        } else {
          rawInvoiceItem = RawBigCInvoiceItem._(name: '');
        }
        rawInvoiceItem.tax = taxMatch.group(1).toString().trim();
      } else {
        final expanedText =
            textBlock.text.replaceAll(_spaceAroundDotRegex, '').trim();
        var priceAndQuantityMatch =
            _priceAndQuantityRegex.firstMatch(expanedText);
        if (priceAndQuantityMatch != null) {
          priceAndQuantityMatch = _priceAndQuantityRegex.firstMatch(
              expanedText.replaceAll(_spaceBetweenNumbersRegex, ''));
          if (rawInvoiceItem != null && priceAndQuantityMatch != null) {
            if (_unitWeightRegex.hasMatch(expanedText)) {
              rawInvoiceItem.weight = priceAndQuantityMatch
                  .group(1)
                  .toString()
                  .replacePatterns([_dotToNon, _commaToDot]);
            } else {
              rawInvoiceItem.quantity = priceAndQuantityMatch
                  .group(1)
                  .toString()
                  .replacePatterns([_commaToDot, _dotToNon]);
            }
            rawInvoiceItem.price = priceAndQuantityMatch
                .group(2)
                .toString()
                .replacePatterns([_commaToDot, _dotToNon]);
          }
        } else {
          final itemMatch = _itemRegex.firstMatch(expanedText);
          if (itemMatch != null) {
            rawInvoiceItem = RawBigCInvoiceItem._(
              name: itemMatch.group(1).toString().trim(),
            );
            rawInvoiceItem.totalPrice = itemMatch
                .group(2)
                .toString()
                .replacePatterns([_commaToDot, _dotToNon]);
          }
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? totalPrice, price, weight;
      int? tax, quantity;
      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _weight = e.weight;
      if (_weight != null) {
        weight = double.tryParse(_weight);
      }
      var _tax = e.tax;
      if (_tax != null) {
        tax = int.tryParse(_tax);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        quantity = int.tryParse(_quantity);
      }
      return InvoiceItem(
        code: e.code ?? '',
        name: e.name,
        totalPrice: totalPrice,
        price: price,
        tax: tax,
        weight: weight,
        quantity: quantity,
      );
    }).toList();

    return Invoice(type: type, number: invoiceNumber, items: invoiceItems);
  }
}

class RawBigCInvoiceItem {
  final String name;
  String? code;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondaryTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;

  TextBlock? get secondary => _secondaryTextBlock;

  TextBlock? get extra => _extraTextBlock;

  RawBigCInvoiceItem._({required this.name});

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondaryTextBlock != null) {
      _secondaryTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

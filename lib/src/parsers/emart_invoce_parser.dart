import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class EmartInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.Emart;

  final _invoiceDate = RegExp(
      r'\b((?<D>\d{1,2})\-(?<M>\d{1,2})\-(?<Y>\d{4})\s(?<h>\d{2})\:(?<m>\d{2}))\b');
  final _invoiceNumberRegex =
      RegExp(r'([\dODUIZzBAqSsGT/Rg]{4}\-[\dODUIZzBAqSsGT/Rg]{4})\b');
  final _itemRegex = RegExp(r'([\d\w]+)(?=\)).+(?<=VAT\s?)([\w\d]+)\s+(.+)');
  final _priceAndQuantityRegex =
      RegExp(r'([\d\w]{1,3}(?:[.,][\d\w]{3})*(?:[.,][\d\w]{2})?)');

  final _notDigitToNon = ReplacePattern(from: RegExp(r'(\D)'), to: '');
  final _upperOTo0 = ReplacePattern(from: 'O', to: '0');
  final _upperITo1 = ReplacePattern(from: 'I', to: '1');
  final _upperZTo2 = ReplacePattern(from: 'Z', to: '2');
  final _upperSTo5 = ReplacePattern(from: 'S', to: '5');
  final _upperGTo6 = ReplacePattern(from: 'G', to: '6');
  final _upperTorSlashTo7 = ReplacePattern(from: RegExp(r'T|\/'), to: '7');
  final _upperRTo8 = ReplacePattern(from: 'R', to: '8');
  final _lowerGTo9 = ReplacePattern(from: 'g', to: '9');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawEmartInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks = prepareTextBlockAccordingLine(recognisedText);
    final rawInvoiceItems = <RawEmartInvoiceItem>[];

    final prepareNumbers = [
      _upperOTo0,
      _upperITo1,
      _upperZTo2,
      _upperSTo5,
      _upperGTo6,
      _upperTorSlashTo7,
      _upperRTo8,
      _lowerGTo9,
      _notDigitToNon,
    ];

    DateTime? invoiceDate;
    String? invoiceNumber;
    RawEmartInvoiceItem? rawInvoiceItem;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }

      final expanedText = textBlock.text;
      print(expanedText);

      if (invoiceDate == null) {
        final invoiceDateMatch = _invoiceDate.firstMatch(expanedText);
        if (invoiceDateMatch != null) {
          invoiceDate = DateTime(
            int.parse(invoiceDateMatch.namedGroup('Y').toString()),
            int.parse(invoiceDateMatch.namedGroup('M').toString()),
            int.parse(invoiceDateMatch.namedGroup('D').toString()),
            int.parse(invoiceDateMatch.namedGroup('h').toString()),
            int.parse(invoiceDateMatch.namedGroup('m').toString()),
          );
        }
      }

      if (invoiceNumber == null && rawInvoiceItems.isEmpty) {
        final invoiceNumberMatch =
            _invoiceNumberRegex.firstMatch(textBlock.text);
        if (invoiceNumberMatch != null) {
          invoiceNumber =
              invoiceNumberMatch.group(1).toString().trim().replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
        }
      }

      final itemMatch = _itemRegex.firstMatch(expanedText);
      if (itemMatch != null) {
        final name = itemMatch.group(3).toString().trim();
        rawInvoiceItem = RawEmartInvoiceItem._(name: name);
        String vat = itemMatch
            .group(2)
            .toString()
            .replacePatterns(prepareNumbers)
            .trim();
        if (vat.length != 2) {
          if (vat.startsWith('1')) {
            vat = '10';
          } else if (vat.startsWith('0')) {
            vat = '05';
          }
        }
        rawInvoiceItem.tax = vat;
      } else if (rawInvoiceItem != null) {
        if (textBlock.lines.length > 1) {
          rawInvoiceItem.code =
              textBlock.lines.first.text.replacePatterns(prepareNumbers).trim();
          final remainedText = textBlock.lines
              .skip(1)
              .fold<String>("", (val, e) => "$val ${e.text}");
          final matches = _priceAndQuantityRegex
              .allMatches(remainedText)
              .map((e) =>
                  e.group(1).toString().replacePatterns(prepareNumbers).trim())
              .toList();
          switch (matches.length) {
            case 1:
              rawInvoiceItem.price = matches[0];
              break;
            case 2:
              rawInvoiceItem.price = matches[0];
              rawInvoiceItem.totalPrice = matches[1];
              break;
            case 3:
              rawInvoiceItem.price = matches[0];
              rawInvoiceItem.quantity = matches[1];
              rawInvoiceItem.totalPrice = matches[2];
              break;
          }
        } else {
          String weight = textBlock.lines.first.text.trim();
          if (weight.endsWith('9') || weight.endsWith('g')) {
            weight = weight.substring(0, weight.length - 1);
          }
          rawInvoiceItem.weight = weight.replacePatterns(prepareNumbers);
        }

        if (rawInvoiceItem.price != null && rawInvoiceItem.weight != null) {
          rawInvoiceItem.quantity = "1";
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? totalPrice, price, weight;
      int? tax, quantity;
      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _weight = e.weight;
      if (_weight != null) {
        weight = double.tryParse(_weight);
      }
      var _tax = e.tax;
      if (_tax != null) {
        tax = int.tryParse(_tax);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        quantity = int.tryParse(_quantity);
      }
      return InvoiceItem(
        code: e.code ?? '',
        name: e.name,
        totalPrice: totalPrice,
        price: price,
        tax: tax,
        weight: weight,
        quantity: quantity,
      );
    }).toList();

    return Invoice(
      type: type,
      date: invoiceDate,
      number: invoiceNumber,
      items: invoiceItems,
    );
  }
}

class RawEmartInvoiceItem {
  final String name;
  String? code;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondaryTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;
  TextBlock? get secondary => _secondaryTextBlock;
  TextBlock? get extra => _extraTextBlock;

  RawEmartInvoiceItem._({required this.name});

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondaryTextBlock != null) {
      _secondaryTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class VinmartInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.Vinmart;

  final _invoiceDate = RegExp(
      r'((?<D>\d{2})\/(?<M>\d{2})\/(?<Y>\d{4})\s(?<h>\d{2})\:(?<m>\d{2}))\b');
  final _invoiceNumberRegex = RegExp(r'(?<=\:\s+?)([\dODUIZzBAqSsGT/Rg]{8})\b');
  final _itemRegex = RegExp(r'^([^:"]+?)$');
  final _itemCodeAndQuantityAndPricesRegex = RegExp(
      r'^((?:\s*[\d]){13})\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*)\s+((?:\s*[\d]){1,3})\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*)$');
  final _itemCodeAndPricesRegex = RegExp(
      r'^((?:\s*[\d]){13})\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*)\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*)$');

  final _doubleDotToDot = ReplacePattern(from: '..', to: '.');
  final _dotAfterSpaceToNon = ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterDotToNon =
      ReplacePattern(from: RegExp(r'(?<=\.)\s+'), to: '');

  final _doubleCommaToComma = ReplacePattern(from: ',,', to: ',');
  final _commaAfterSpaceToNon =
      ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterCommaToNon =
      ReplacePattern(from: RegExp(r'(?<=,)\s+'), to: '');

  final _dotOrCommaToNon = ReplacePattern(from: RegExp(r'\.|,'), to: '');
  final _spaceBetweenNumberToNon =
      ReplacePattern(from: RegExp(r'(?<=\d)\s+(?=\d)'), to: '');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawVinmartInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks = prepareTextBlockAccordingLine(recognisedText);
    final rawInvoiceItems = <RawVinmartInvoiceItem>[];

    DateTime? invoiceDate;
    String? invoiceNumber;
    RawVinmartInvoiceItem? rawInvoiceItem;

    String? prevExpanedText;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }

      final expanedText = textBlock.text;
      print(expanedText);

      if (invoiceDate == null) {
        final invoiceDateMatch = _invoiceDate.firstMatch(expanedText);
        if (invoiceDateMatch != null) {
          invoiceDate = DateTime(
            int.parse(invoiceDateMatch.namedGroup('Y').toString()),
            int.parse(invoiceDateMatch.namedGroup('M').toString()),
            int.parse(invoiceDateMatch.namedGroup('D').toString()),
            int.parse(invoiceDateMatch.namedGroup('h').toString()),
            int.parse(invoiceDateMatch.namedGroup('m').toString()),
            int.parse(invoiceDateMatch.namedGroup('s').toString()),
          );
        }
      }

      if (invoiceNumber == null && rawInvoiceItems.isEmpty) {
        final invoiceNumberMatch =
            _invoiceNumberRegex.firstMatch(textBlock.text);
        if (invoiceNumberMatch != null) {
          invoiceNumber =
              invoiceNumberMatch.group(1).toString().trim().replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
        }
      }

      final preparedNumericText = expanedText.replacePatterns([
        _prepareNum0,
        _prepareNum1,
        _prepareNum2,
        _prepareNum3,
        _prepareNum4,
        _prepareNum5,
        _prepareNum6,
        _prepareNum7,
        _prepareNum8,
        _prepareNum9,
        _spaceAfterCommaToNon,
        _commaAfterSpaceToNon,
        _doubleCommaToComma,
        _spaceAfterDotToNon,
        _dotAfterSpaceToNon,
        _doubleDotToDot,
      ]);

      final itemCodeAndQuantityAndPricesMatch =
          _itemCodeAndQuantityAndPricesRegex.firstMatch(preparedNumericText);
      if (itemCodeAndQuantityAndPricesMatch != null) {
        rawInvoiceItem = RawVinmartInvoiceItem._();
        rawInvoiceItem.code = itemCodeAndQuantityAndPricesMatch
            .group(1)
            .toString()
            .trim()
            .replacePatterns([
          _spaceBetweenNumberToNon,
        ]);
        rawInvoiceItem.price = itemCodeAndQuantityAndPricesMatch
            .group(2)
            .toString()
            .trim()
            .replacePatterns([
          _dotOrCommaToNon,
          _spaceBetweenNumberToNon,
        ]);
        rawInvoiceItem.quantity = itemCodeAndQuantityAndPricesMatch
            .group(3)
            .toString()
            .trim()
            .replacePatterns([
          _dotOrCommaToNon,
          _spaceBetweenNumberToNon,
        ]);
        rawInvoiceItem.totalPrice = itemCodeAndQuantityAndPricesMatch
            .group(4)
            .toString()
            .trim()
            .replacePatterns([
          _dotOrCommaToNon,
          _spaceBetweenNumberToNon,
        ]);

        if (prevExpanedText != null) {
          final itemMatch = _itemRegex.firstMatch(prevExpanedText);
          if (itemMatch != null) {
            rawInvoiceItem.name = itemMatch.group(1).toString().trim();
          }
        }
      } else {
        final _itemCodeAndPricesMatch =
            _itemCodeAndPricesRegex.firstMatch(preparedNumericText);
        if (_itemCodeAndPricesMatch != null) {
          rawInvoiceItem = RawVinmartInvoiceItem._();
          rawInvoiceItem.code = _itemCodeAndPricesMatch
              .group(1)
              .toString()
              .trim()
              .replacePatterns([
            _spaceBetweenNumberToNon,
          ]);
          rawInvoiceItem.price = _itemCodeAndPricesMatch
              .group(2)
              .toString()
              .trim()
              .replacePatterns([
            _dotOrCommaToNon,
            _spaceBetweenNumberToNon,
          ]);
          rawInvoiceItem.totalPrice = _itemCodeAndPricesMatch
              .group(3)
              .toString()
              .trim()
              .replacePatterns([
            _dotOrCommaToNon,
            _spaceBetweenNumberToNon,
          ]);

          if (prevExpanedText != null) {
            final itemMatch = _itemRegex.firstMatch(prevExpanedText);
            if (itemMatch != null) {
              rawInvoiceItem.name = itemMatch.group(1).toString().trim();
            }
          }
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }

      prevExpanedText = expanedText;
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? totalPrice, price;
      int? quantity;

      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        quantity = int.tryParse(_quantity);
      } else if (totalPrice != null && price != null) {
        if (totalPrice.compareTo(price) == 0) {
          quantity = 1;
        }
      }

      return InvoiceItem(
        code: e.code ?? '',
        name: e.name ?? '',
        totalPrice: totalPrice,
        price: price,
        quantity: quantity,
      );
    }).toList();

    return Invoice(
      type: type,
      date: invoiceDate,
      number: invoiceNumber,
      items: invoiceItems,
    );
  }
}

class RawVinmartInvoiceItem {
  String? name;
  String? code;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondaryTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;

  TextBlock? get secondary => _secondaryTextBlock;

  TextBlock? get extra => _extraTextBlock;

  RawVinmartInvoiceItem._();

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondaryTextBlock != null) {
      _secondaryTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class GoInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.Go;

  final _invoiceDate = RegExp(
      r'((?<D>\d{2})\/(?<M>\d{2})\/(?<Y>\d{4})\s(?<h>\d{2})\:(?<m>\d{2})\:(?<s>\d{2}))\b');
  final _invoiceNumberRegex =
      RegExp(r'(?<=[\dODUIZzBAqSsGT/Rg]{6}\s+?)([\dODUIZzBAqSsGT/Rg]{9})\b');

  final _itemRegex = RegExp(r'^([^,:]+)\s([IiZz12]{1})$');
  final _itemPricesRegex =
      RegExp(r'(?:x|X)\s(\b((?:\d){1,3}(?:[.,](?:\d){3})+)?\s?)+');
  final _itemQuantityRegex = RegExp(r'^((?:\d){1,3}(?:(?:[.,](?:\d){3})+)?)\b');

  final _commaOrDotRegex = RegExp(r'(\.|,)');

  final _commaToDot = ReplacePattern(from: ',', to: '.');
  final _commaOrDotToNon = ReplacePattern(from: RegExp(r'(\.|,)'), to: '');

  final _spaceAfterDotToNon =
      ReplacePattern(from: RegExp(r'(?<=\.)\s+'), to: '');
  final _spaceBetweenNumberToNon =
      ReplacePattern(from: RegExp(r'(?<=\d)\s+(?=\d)'), to: '');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawGoInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks =
        prepareTextBlockAccordingLine(recognisedText, ratio: 1.5);
    final rawInvoiceItems = <RawGoInvoiceItem>[];

    DateTime? invoiceDate;
    String? invoiceNumber;
    RawGoInvoiceItem? rawInvoiceItem;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }

      final expanedText = textBlock.text;
      print(expanedText);

      if (invoiceDate == null) {
        final invoiceDateMatch = _invoiceDate.firstMatch(expanedText);
        if (invoiceDateMatch != null) {
          invoiceDate = DateTime(
            int.parse(invoiceDateMatch.namedGroup('Y').toString()),
            int.parse(invoiceDateMatch.namedGroup('M').toString()),
            int.parse(invoiceDateMatch.namedGroup('D').toString()),
            int.parse(invoiceDateMatch.namedGroup('h').toString()),
            int.parse(invoiceDateMatch.namedGroup('m').toString()),
            int.parse(invoiceDateMatch.namedGroup('s').toString()),
          );
        }
      }

      if (invoiceNumber == null) {
        final invoiceNumberMatch = _invoiceNumberRegex.firstMatch(expanedText);
        if (invoiceNumberMatch != null) {
          invoiceNumber =
              invoiceNumberMatch.group(1).toString().trim().replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
          continue;
        }
      }

      final itemMatch = _itemRegex.firstMatch(expanedText);
      if (itemMatch != null) {
        rawInvoiceItem =
            RawGoInvoiceItem._(name: itemMatch.group(1).toString().trim());
        rawInvoiceItem.tax =
            itemMatch.group(2).toString().trim().replacePatterns([
          _prepareNum1,
          _prepareNum2,
        ]);
      } else if (rawInvoiceItem != null) {
        final preparedText = expanedText.replacePatterns([
          _commaToDot,
          _spaceAfterDotToNon,
          _spaceBetweenNumberToNon,
        ]);
        final pricesMatch = _itemPricesRegex.firstMatch(preparedText);
        if (pricesMatch != null) {
          rawInvoiceItem.price = pricesMatch
              .group(1)
              .toString()
              .trim()
              .replacePatterns([_commaOrDotToNon]);
          rawInvoiceItem.totalPrice = pricesMatch
              .group(2)
              .toString()
              .trim()
              .replacePatterns([_commaOrDotToNon]);
          final quantityMatch = _itemQuantityRegex
              .firstMatch(preparedText.substring(0, pricesMatch.start));
          if (quantityMatch != null) {
            final quantity = quantityMatch.group(1).toString().trim();
            final decimals = quantity.split('.');
            if (_commaOrDotRegex.hasMatch(quantity) &&
                decimals.length > 0 &&
                decimals.last != '000') {
              rawInvoiceItem.weight = quantity;
              print('--------------------------------------------------------');
              print('${rawInvoiceItem.price} - ${rawInvoiceItem.totalPrice}');
              print(preparedText);
              print('--------------------------------------------------------');
            } else {
              rawInvoiceItem.quantity = quantity;
            }
          }
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? totalPrice, price, weight;
      int? tax, quantity;
      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _weight = e.weight;
      if (_weight != null) {
        weight = double.tryParse(_weight);
      }
      var _tax = e.tax;
      if (_tax != null) {
        tax = int.tryParse(_tax);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        quantity = int.tryParse(_quantity);
      }
      return InvoiceItem(
        code: e.code ?? '',
        name: e.name,
        totalPrice: totalPrice,
        price: price,
        tax: tax,
        weight: weight,
        quantity: quantity,
      );
    }).toList();

    return Invoice(
      type: type,
      date: invoiceDate,
      number: invoiceNumber,
      items: invoiceItems,
    );
  }
}

class RawGoInvoiceItem {
  final String name;
  String? code;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondaryTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;

  TextBlock? get secondary => _secondaryTextBlock;

  TextBlock? get extra => _extraTextBlock;

  RawGoInvoiceItem._({required this.name});

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondaryTextBlock != null) {
      _secondaryTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

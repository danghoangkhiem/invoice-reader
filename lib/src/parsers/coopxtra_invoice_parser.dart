import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class CoopxtraInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.CoopXtra;

  final _invoiceDate = RegExp(
      r'((?<D>\d{1,2})\/(?<M>\d{1,2})\/(?<Y>\d{4})\s(?<h>\d{2})\:(?<m>\d{2})\:(?<s>\d{2}))\b');
  final _invoiceNumberRegex =
      RegExp(r'(?<=[OQD]\:\s+?)((?:\s*[\dODUIZzBAqSsGT/Rg]{5}))$');

  final _item1Regex = RegExp(r'^((?:\s*[\dODUIZzBAqSsGT/Rg]){14})\s+([^:]+?)$');
  final _item2Regex = RegExp(r'^((?:\s*[\dODUIZzBAqSsGT/Rg]){13})\s+([^:]+?)$');
  final _item1PricesRegex = RegExp(
      r'(?<=\s+?)((?:\d){1,3}(?:(?:[.,](?:\d){3})+(?:[.,](?:\d){2}))?)\b');
  final _item2PricesRegex =
      RegExp(r'(?<=\s+?)((?:\d){1,3}(?:(?:[.,](?:\d){3})+)?)(?!(?:\.\d{2}))\b');
  final _item1TaxRegex = RegExp(r'(?<=[VAT]{2,3})(\w{2})(?=\b|x|X)');
  final _item2TaxRegex = RegExp(r'(?<=[VAT]{2,3})(\w{1,2})(?=\b|x|X)');
  final _itemWeight =
      RegExp(r'\b((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})+)\s?(?=k|K[gG96])');
  final _item1OriginalPriceRegex = RegExp(
      r'(?<=[g|9|o|O|0|c|C]{2,3}[^.,]\s+)((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})+(?:[.,](?:\s*[\d]){2}))$',
      caseSensitive: false);
  final _item2OriginalPriceRegex = RegExp(
      r'(?<=[g|9|o|O|0|c|C]{2,3}[^.,]\s+)((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})+)$',
      caseSensitive: false);

  final _moneyUnitRegex = RegExp(r'(?<=\d{3}\s*)(4|d)\b');
  final _decimalPartRegex = RegExp(r'[,.](?=\d{2}\b)');

  final _spaceBetweenNumberToNon =
      ReplacePattern(from: RegExp(r'(?<=\d)\s+(?=\d)'), to: '');
  final _spaceAfterDotToNon =
      ReplacePattern(from: RegExp(r'(?<=\.)\s+'), to: '');

  final _commaOrDotNotDecimalToNon =
      ReplacePattern(from: RegExp(r'[,.](?=\d{3}\b)'), to: '');
  final _commaOrDotToNon = ReplacePattern(from: RegExp(r'(\.|,)'), to: '');
  final _commaToDot = ReplacePattern(from: ',', to: '.');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawCoopXtraInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks = prepareTextBlockAccordingLine(recognisedText);
    final rawInvoiceItems = <RawCoopXtraInvoiceItem>[];

    DateTime? invoiceDate;
    String? invoiceNumber;
    RawCoopXtraInvoiceItem? rawInvoiceItem;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }

      final expanedText = textBlock.text;
      print(expanedText);

      if (invoiceDate == null) {
        final invoiceDateMatch = _invoiceDate.firstMatch(expanedText);
        if (invoiceDateMatch != null) {
          invoiceDate = DateTime(
            int.parse(invoiceDateMatch.namedGroup('Y').toString()),
            int.parse(invoiceDateMatch.namedGroup('M').toString()),
            int.parse(invoiceDateMatch.namedGroup('D').toString()),
            int.parse(invoiceDateMatch.namedGroup('h').toString()),
            int.parse(invoiceDateMatch.namedGroup('m').toString()),
            int.parse(invoiceDateMatch.namedGroup('s').toString()),
          );
        }
      }

      if (invoiceNumber == null && rawInvoiceItems.isEmpty) {
        final invoiceNumberMatch = _invoiceNumberRegex.firstMatch(expanedText);
        if (invoiceNumberMatch != null) {
          invoiceNumber =
              invoiceNumberMatch.group(1).toString().trim().replacePatterns([
            _spaceBetweenNumberToNon,
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
        }
      }

      final itemMatch = _item1Regex.firstMatch(expanedText) ??
          _item2Regex.firstMatch(expanedText);
      if (itemMatch != null) {
        final code = itemMatch.group(1).toString().trim().replacePatterns([
          _spaceBetweenNumberToNon,
          _prepareNum0,
          _prepareNum1,
          _prepareNum2,
          _prepareNum3,
          _prepareNum4,
          _prepareNum5,
          _prepareNum6,
          _prepareNum7,
          _prepareNum8,
          _prepareNum9,
        ]);
        rawInvoiceItem = RawCoopXtraInvoiceItem._(
          code: code,
          name: itemMatch.group(2).toString().trim(),
        );
      } else if (rawInvoiceItem != null) {
        final taxMatch = _item1TaxRegex.firstMatch(expanedText) ??
            _item2TaxRegex.firstMatch(expanedText);
        if (taxMatch != null) {
          rawInvoiceItem.tax =
              taxMatch.group(1).toString().trim().replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum5,
          ]);
          int startSubstring = taxMatch.end;
          int kind = 1;
          final weightMatch = _itemWeight.firstMatch(expanedText);
          if (weightMatch != null) {
            rawInvoiceItem.weight =
                weightMatch.group(1).toString().trim().replacePatterns([
              _commaToDot,
              _spaceAfterDotToNon,
              _spaceBetweenNumberToNon,
            ]);
            startSubstring = weightMatch.end;
            kind = 2;
          }
          final preparedText =
              expanedText.substring(startSubstring).replacePatterns([
            _commaToDot,
            _spaceAfterDotToNon,
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
          var priceMatches;
          if (kind == 2 || !_decimalPartRegex.hasMatch(preparedText)) {
            priceMatches = _item2PricesRegex
                .allMatches(preparedText.replaceAll(_moneyUnitRegex, ''))
                .toList()
                .map<String>((e) => e
                    .group(1)
                    .toString()
                    .trim()
                    .replacePatterns([_commaOrDotToNon]))
                .toList();
          } else {
            priceMatches = _item1PricesRegex
                .allMatches(preparedText)
                .toList()
                .map<String>((e) => e
                    .group(1)
                    .toString()
                    .trim()
                    .replacePatterns([_commaOrDotNotDecimalToNon]))
                .toList();
          }
          if (priceMatches.length == 3) {
            rawInvoiceItem.quantity = priceMatches[0];
            rawInvoiceItem.price = priceMatches[1];
            rawInvoiceItem.totalPrice = priceMatches[2];
          } else if (priceMatches.length == 2) {
            if (priceMatches[0].length < 3) {
              rawInvoiceItem.quantity = priceMatches[0];
            }
            rawInvoiceItem.price = priceMatches[0];
            rawInvoiceItem.totalPrice = priceMatches[1];
          }
        } else {
          final weightMatch = _itemWeight.firstMatch(expanedText);
          if (weightMatch != null) {
            rawInvoiceItem.weight =
                weightMatch.group(1).toString().trim().replacePatterns([
              _commaToDot,
              _spaceAfterDotToNon,
              _spaceBetweenNumberToNon,
            ]);
          } else {
            final originalPriceMatch =
                _item1OriginalPriceRegex.firstMatch(expanedText);
            if (originalPriceMatch != null) {
              rawInvoiceItem.originalPrice = originalPriceMatch
                  .group(1)
                  .toString()
                  .trim()
                  .replacePatterns([
                _commaToDot,
                _spaceAfterDotToNon,
                _spaceBetweenNumberToNon,
                _commaOrDotNotDecimalToNon,
              ]);
            } else {
              final originalPriceMatch =
                  _item2OriginalPriceRegex.firstMatch(expanedText);
              if (originalPriceMatch != null) {
                rawInvoiceItem.originalPrice = originalPriceMatch
                    .group(1)
                    .toString()
                    .trim()
                    .replacePatterns([
                  _commaOrDotToNon,
                  _spaceBetweenNumberToNon,
                ]);
              }
            }
          }
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? originalPrice, totalPrice, price, weight;
      int? tax, quantity;
      var _originalPrice = e.originalPrice;
      if (_originalPrice != null) {
        originalPrice = double.tryParse(_originalPrice);
      }
      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _weight = e.weight;
      if (_weight != null) {
        weight = double.tryParse(_weight);
      }
      var _tax = e.tax;
      if (_tax != null) {
        tax = int.tryParse(_tax);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        quantity = int.tryParse(_quantity);
      }
      return InvoiceItem(
        code: e.code,
        name: e.name,
        originalPrice: originalPrice,
        totalPrice: totalPrice,
        price: price,
        tax: tax,
        weight: weight,
        quantity: quantity,
      );
    }).toList();

    return Invoice(
      type: type,
      date: invoiceDate,
      number: invoiceNumber,
      items: invoiceItems,
    );
  }
}

class RawCoopXtraInvoiceItem {
  final String code;
  final String name;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondaryTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;

  TextBlock? get secondary => _secondaryTextBlock;

  TextBlock? get extra => _extraTextBlock;

  RawCoopXtraInvoiceItem._({required this.code, required this.name});

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondaryTextBlock != null) {
      _secondaryTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class AeonInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.Aeon;

  final _invoiceDate = RegExp(
      r'((?<h>\d{2})\:(?<m>\d{2})\s(?<D>\d{2})\/(?<M>\d{2})\/(?<Y>\d{4}))\b');
  final _invoiceNumberRegex =
      RegExp(r'(?<=CH\:\s+?)([\dODUIZzBAqSsGT/Rg]{7})\b');
  final _itemRegex = RegExp(r'^([^:]+?)\s+((?:\s*[\dODUIZzBAqSsGT/Rg]){12})$');
  final _itemQuantityAndPricesRegex = RegExp(
      r'([\dODUIZzBAqSsGT/Rg]{1,3}(?:[.,][\dODUIZzBAqSsGT/Rg]{3})*)\s+([\dODUIZzBAqSsGT/Rg]{1,3}(?:[.,][\dODUIZzBAqSsGT/Rg]{3})*)\s+([\dODUIZzBAqSsGT/Rg]{1,3}(?:[.,][\dODUIZzBAqSsGT/Rg]{3})*)\b');

  final _dotOrCommaRegex = RegExp(r'\.|,');

  final _dotToNon = ReplacePattern(from: '.', to: '');
  final _doubleDotToDot = ReplacePattern(from: '..', to: '.');
  final _dotAfterSpaceToNon = ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterDotToNon =
      ReplacePattern(from: RegExp(r'(?<=\.)\s+'), to: '');

  final _commaToNon = ReplacePattern(from: ',', to: '');
  final _doubleCommaToComma = ReplacePattern(from: ',,', to: ',');
  final _commaAfterSpaceToNon =
      ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterCommaToNon =
      ReplacePattern(from: RegExp(r'(?<=,)\s+'), to: '');

  final _spaceBetweenNumberToNon =
      ReplacePattern(from: RegExp(r'(?<=\d)\s+(?=\d)'), to: '');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawAeonInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks = prepareTextBlockAccordingLine(recognisedText);
    final rawInvoiceItems = <RawAeonInvoiceItem>[];

    DateTime? invoiceDate;
    String? invoiceNumber;
    RawAeonInvoiceItem? rawInvoiceItem;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }

      final expanedText = textBlock.text;
      print(expanedText);

      if (invoiceDate == null) {
        final invoiceDateMatch = _invoiceDate.firstMatch(expanedText);
        if (invoiceDateMatch != null) {
          invoiceDate = DateTime(
            int.parse(invoiceDateMatch.namedGroup('Y').toString()),
            int.parse(invoiceDateMatch.namedGroup('M').toString()),
            int.parse(invoiceDateMatch.namedGroup('D').toString()),
            int.parse(invoiceDateMatch.namedGroup('h').toString()),
            int.parse(invoiceDateMatch.namedGroup('m').toString()),
          );
        }
      }

      if (invoiceNumber == null) {
        final invoiceNumberMatch = _invoiceNumberRegex.firstMatch(expanedText);
        if (invoiceNumberMatch != null) {
          invoiceNumber =
              invoiceNumberMatch.group(1).toString().trim().replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
          ]);
        }
      }

      final itemMatch = _itemRegex.firstMatch(expanedText);
      if (itemMatch != null) {
        final name = itemMatch.group(1).toString().trim();
        final code = itemMatch.group(2).toString().trim().replacePatterns([
          _prepareNum0,
          _prepareNum1,
          _prepareNum2,
          _prepareNum3,
          _prepareNum4,
          _prepareNum5,
          _prepareNum6,
          _prepareNum7,
          _prepareNum8,
          _prepareNum9,
          _spaceBetweenNumberToNon,
        ]);
        rawInvoiceItem = RawAeonInvoiceItem._(code: code, name: name);
      } else if (rawInvoiceItem != null) {
        final itemQuantityAndPricesMatch =
            _itemQuantityAndPricesRegex.firstMatch(
          expanedText.replacePatterns([
            _prepareNum0,
            _prepareNum1,
            _prepareNum2,
            _prepareNum3,
            _prepareNum4,
            _prepareNum5,
            _prepareNum6,
            _prepareNum7,
            _prepareNum8,
            _prepareNum9,
            _spaceAfterCommaToNon,
            _commaAfterSpaceToNon,
            _doubleCommaToComma,
            _spaceAfterDotToNon,
            _dotAfterSpaceToNon,
            _doubleDotToDot,
          ]),
        );
        if (itemQuantityAndPricesMatch != null) {
          final firstNumber =
              itemQuantityAndPricesMatch.group(1).toString().trim();
          if (firstNumber.contains(_dotOrCommaRegex)) {
            rawInvoiceItem.weight =
                firstNumber.replaceAll(_dotOrCommaRegex, '.');
          } else {
            rawInvoiceItem.quantity = firstNumber;
          }
          rawInvoiceItem.price = itemQuantityAndPricesMatch
              .group(2)
              .toString()
              .trim()
              .replacePatterns([_dotToNon, _commaToNon]);
          rawInvoiceItem.totalPrice = itemQuantityAndPricesMatch
              .group(3)
              .toString()
              .trim()
              .replacePatterns([_dotToNon, _commaToNon]);
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? originalPrice, totalPrice, price, weight;
      int? tax, quantity;
      var _originalPrice = e.originalPrice;
      if (_originalPrice != null) {
        originalPrice = double.tryParse(_originalPrice);
      }
      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _weight = e.weight;
      if (_weight != null) {
        weight = double.tryParse(_weight);
      }
      var _tax = e.tax;
      if (_tax != null) {
        tax = int.tryParse(_tax);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        quantity = int.tryParse(_quantity);
      }
      return InvoiceItem(
        code: e.code,
        name: e.name,
        originalPrice: originalPrice,
        totalPrice: totalPrice,
        price: price,
        tax: tax,
        weight: weight,
        quantity: quantity,
      );
    }).toList();

    return Invoice(
      type: type,
      number: invoiceNumber,
      date: invoiceDate,
      items: invoiceItems,
    );
  }
}

class RawAeonInvoiceItem {
  final String name;
  final String code;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondaryTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;

  TextBlock? get secondary => _secondaryTextBlock;

  TextBlock? get extra => _secondaryTextBlock;

  RawAeonInvoiceItem._({required this.code, required this.name});

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondaryTextBlock != null) {
      _secondaryTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

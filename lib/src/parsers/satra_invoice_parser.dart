import 'package:invoice_reader/invoice_reader.dart';

import '../utils/string_extensions.dart';

class SatraInvoiceParser extends InvoiceParser {
  @override
  InvoiceType get type => InvoiceType.Satra;

  final _invoiceDate = RegExp(
      r'((?<D>\d{2})\/(?<M>\d{2})\/(?<Y>\d{4})\s(?<h>\d{2})\:(?<m>\d{2}))\b');
  final _invoiceNumberRegex = RegExp(r'(?<=\:\s+?)(\w{14})\b');
  final _itemRegex = RegExp(r'^((?:\s*\d)+)\s+([^:"]+?)$');
  final _itemQuantityAndPricesRegex = RegExp(
      r'^((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*(?:[.,](?:\s*[\d]){2}))\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){2,3})*)\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*(?:[.,](?:\s*[\d]){2}))$');
  final _itemPricesRegex = RegExp(
      r'^((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*(?:[.,](?:\s*[\d]){2}))\s+((?:\s*[\d]){1,3}(?:[.,](?:\s*[\d]){3})*(?:[.,](?:\s*[\d]){2}))$');
  final _itemCodeAndTax = RegExp(
      r'^((?:\s*[\dODUIZzBAqSsGT/Rg]){13})(?:.+?)((?:\s*[\dODUISs]){2})?$');

  final _doubleDotToDot = ReplacePattern(from: '..', to: '.');
  final _dotAfterSpaceToNon = ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterDotToNon =
      ReplacePattern(from: RegExp(r'(?<=\.)\s+'), to: '');

  final _doubleCommaToComma = ReplacePattern(from: ',,', to: ',');
  final _commaAfterSpaceToNon =
      ReplacePattern(from: RegExp(r'\s(?=,)'), to: '');
  final _spaceAfterCommaToNon =
      ReplacePattern(from: RegExp(r'(?<=,)\s+'), to: '');

  final _dotOrCommaToDot = ReplacePattern(from: RegExp(r'\.|,'), to: '.');
  final _notLastFractionToNon =
      ReplacePattern(from: RegExp(r'(,|\.)(?!\d{2}\b)'), to: '');
  final _spaceBetweenNumberToNon =
      ReplacePattern(from: RegExp(r'(?<=\d)\s+(?=\d)'), to: '');

  final _prepareNum0 = ReplacePattern(from: RegExp(r'O|D|U'), to: '0');
  final _prepareNum1 = ReplacePattern(from: 'I', to: '1');
  final _prepareNum2 = ReplacePattern(from: RegExp(r'Z|z'), to: '2');
  final _prepareNum3 = ReplacePattern(from: 'B', to: '3');
  final _prepareNum4 = ReplacePattern(from: RegExp(r'A|q'), to: '4');
  final _prepareNum5 = ReplacePattern(from: RegExp(r'S|s'), to: '5');
  final _prepareNum6 = ReplacePattern(from: 'G', to: '6');
  final _prepareNum7 = ReplacePattern(from: RegExp(r'T|/'), to: '7');
  final _prepareNum8 = ReplacePattern(from: 'R', to: '8');
  final _prepareNum9 = ReplacePattern(from: 'g', to: '9');

  final List<RawSatraInvoiceItem> raws = [];

  @override
  Invoice parse(RecognisedText recognisedText) {
    final textBlocks = prepareTextBlockAccordingLine(recognisedText);
    final rawInvoiceItems = <RawSatraInvoiceItem>[];

    DateTime? invoiceDate;
    String? invoiceNumber;
    RawSatraInvoiceItem? rawInvoiceItem;

    for (int i = 0; i < textBlocks.length; i++) {
      final textBlock = textBlocks[i];
      if (textBlock.lines.isEmpty) {
        continue;
      }

      final expanedText = textBlock.text;
      print(expanedText);

      if (invoiceDate == null) {
        final invoiceDateMatch = _invoiceDate.firstMatch(expanedText);
        if (invoiceDateMatch != null) {
          invoiceDate = DateTime(
            int.parse(invoiceDateMatch.namedGroup('Y').toString()),
            int.parse(invoiceDateMatch.namedGroup('M').toString()),
            int.parse(invoiceDateMatch.namedGroup('D').toString()),
            int.parse(invoiceDateMatch.namedGroup('h').toString()),
            int.parse(invoiceDateMatch.namedGroup('m').toString()),
          );
        }
      }

      if (invoiceNumber == null && rawInvoiceItems.isEmpty) {
        final invoiceNumberMatch =
            _invoiceNumberRegex.firstMatch(textBlock.text);
        if (invoiceNumberMatch != null) {
          invoiceNumber = invoiceNumberMatch.group(1).toString().trim();
        }
      }

      final itemMatch = _itemRegex.firstMatch(expanedText);
      if (itemMatch != null) {
        rawInvoiceItem =
            RawSatraInvoiceItem._(name: itemMatch.group(2).toString().trim());
      } else if (rawInvoiceItem != null) {
        final preparedText = expanedText.replacePatterns([
          _prepareNum0,
          _prepareNum1,
          _prepareNum2,
          _prepareNum3,
          _prepareNum4,
          _prepareNum5,
          _prepareNum6,
          _prepareNum7,
          _prepareNum8,
          _prepareNum9,
          _spaceAfterCommaToNon,
          _commaAfterSpaceToNon,
          _doubleCommaToComma,
          _spaceAfterDotToNon,
          _dotAfterSpaceToNon,
          _doubleDotToDot,
        ]);
        final itemQuantityAndPricesMatch =
            _itemQuantityAndPricesRegex.firstMatch(preparedText);
        if (itemQuantityAndPricesMatch != null) {
          rawInvoiceItem.price = itemQuantityAndPricesMatch
              .group(1)
              .toString()
              .trim()
              .replacePatterns([
            _notLastFractionToNon,
            _dotOrCommaToDot,
            _spaceBetweenNumberToNon
          ]);
          rawInvoiceItem.quantity = itemQuantityAndPricesMatch
              .group(2)
              .toString()
              .replacePatterns([_dotOrCommaToDot]).trim();
          rawInvoiceItem.totalPrice = itemQuantityAndPricesMatch
              .group(3)
              .toString()
              .trim()
              .replacePatterns([
            _notLastFractionToNon,
            _dotOrCommaToDot,
            _spaceBetweenNumberToNon
          ]);
        } else {
          final itemPricesMatch = _itemPricesRegex.firstMatch(preparedText);
          if (itemPricesMatch != null) {
            rawInvoiceItem.price = itemPricesMatch
                .group(1)
                .toString()
                .trim()
                .replacePatterns([
              _notLastFractionToNon,
              _dotOrCommaToDot,
              _spaceBetweenNumberToNon
            ]);
            rawInvoiceItem.totalPrice = itemPricesMatch
                .group(2)
                .toString()
                .trim()
                .replacePatterns([
              _notLastFractionToNon,
              _dotOrCommaToDot,
              _spaceBetweenNumberToNon
            ]);
          } else {
            final itemCodeAndTaxMatch = _itemCodeAndTax.firstMatch(expanedText);
            if (itemCodeAndTaxMatch != null) {
              rawInvoiceItem.code = itemCodeAndTaxMatch
                  .group(1)
                  .toString()
                  .trim()
                  .replacePatterns([
                _prepareNum0,
                _prepareNum1,
                _prepareNum2,
                _prepareNum3,
                _prepareNum4,
                _prepareNum5,
                _prepareNum6,
                _prepareNum7,
                _prepareNum8,
                _prepareNum9,
                _spaceAfterCommaToNon,
                _commaAfterSpaceToNon,
                _doubleCommaToComma,
                _spaceAfterDotToNon,
                _dotAfterSpaceToNon,
                _doubleDotToDot,
                _spaceBetweenNumberToNon
              ]);
              final tax = itemCodeAndTaxMatch.group(2);
              if (tax != null) {
                rawInvoiceItem.tax = tax.trim().replacePatterns([
                  _prepareNum0,
                  _prepareNum1,
                  _prepareNum5,
                  _spaceBetweenNumberToNon
                ]);
              }
            }
          }
        }
      }

      if (rawInvoiceItem != null) {
        rawInvoiceItem.addBlock(textBlock);
        if (!rawInvoiceItems.contains(rawInvoiceItem)) {
          rawInvoiceItems.add(rawInvoiceItem);
        }
      }
    }

    raws.clear();
    raws.addAll(List.unmodifiable(rawInvoiceItems));

    final invoiceItems = rawInvoiceItems.map((e) {
      double? originalPrice, totalPrice, price, weight;
      int? tax, quantity;
      var _originalPrice = e.originalPrice;
      if (_originalPrice != null) {
        originalPrice = double.tryParse(_originalPrice);
      }
      var _totalPrice = e.totalPrice;
      if (_totalPrice != null) {
        totalPrice = double.tryParse(_totalPrice);
      }
      var _price = e.price;
      if (_price != null) {
        price = double.tryParse(_price);
      }
      var _quantity = e.quantity;
      if (_quantity != null) {
        final parsed = double.tryParse(_quantity);
        if (parsed != null) {
          if (parsed > parsed.toInt()) {
            weight = parsed;
          } else {
            quantity = parsed.toInt();
          }
        }
      }
      var _tax = e.tax;
      if (_tax != null) {
        tax = int.tryParse(_tax);
      }
      return InvoiceItem(
        code: e.code ?? '',
        name: e.name,
        originalPrice: originalPrice,
        totalPrice: totalPrice,
        price: price,
        tax: tax,
        weight: weight,
        quantity: quantity,
      );
    }).toList();

    return Invoice(
      type: type,
      date: invoiceDate,
      number: invoiceNumber,
      items: invoiceItems,
    );
  }
}

class RawSatraInvoiceItem {
  final String name;
  String? code;
  String? originalPrice;
  String? totalPrice;
  String? price;
  String? tax;
  String? weight;
  String? quantity;

  TextBlock? _primaryTextBlock;
  TextBlock? _secondaryTextBlock;
  TextBlock? _extraTextBlock;

  TextBlock? get primary => _primaryTextBlock;

  TextBlock? get secondary => _secondaryTextBlock;

  TextBlock? get extra => _extraTextBlock;

  RawSatraInvoiceItem._({required this.name});

  void addBlock(TextBlock block) {
    if (_primaryTextBlock != null) {
      _primaryTextBlock = block;
    } else if (_secondaryTextBlock != null) {
      _secondaryTextBlock = block;
    } else if (_extraTextBlock != null) {
      _extraTextBlock = block;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'name': name,
      'originalPrice': originalPrice,
      'totalPrice': totalPrice,
      'price': price,
      'tax': tax,
      'weight': weight,
      'quantity': quantity,
    };
  }
}

part of invoice_reader;

class DocumentDetector {
  DocumentDetector._();

  Future<DocumentDetectorResult> detectDocument(Uint8List bytes) async {
    final result = await InvoiceReaderChannel.invokeMethod(
        'documentDetector#detectDocument', <String, dynamic>{
      'byteData': bytes,
    });
    return DocumentDetectorResult.fromMap(result);
  }

  Future<Uint8List> enhancePicture(Uint8List bytes) async {
    final result = await InvoiceReaderChannel.invokeMethod(
        'documentDetector#enhancePicture', <String, dynamic>{
      'byteData': bytes,
    });
    return result;
  }
}

class DocumentDetectorResult {
  final List<Offset> points;
  final Size size;
  final Uint8List document;

  DocumentDetectorResult.fromMap(Map<dynamic, dynamic> map)
      : points = map['points'] != null
            ? List.from(map['points'])
                .map((e) => Offset(e['x'], e['y']))
                .toList()
            : const [],
        size = map['size'] != null
            ? Size(map['size']['width'], map['size']['height'])
            : Size.zero,
        document = map['document'];
}

part of invoice_reader;

abstract class InvoiceParser {
  InvoiceType get type;

  Invoice parse(RecognisedText recognisedText);

  @protected
  List<TextBlock> prepareTextBlockAccordingLine(
    RecognisedText recognisedText, {
    double ratio = 2.0,
  }) {
    final blocks = <TextBlock>[];
    final expandedLines =
        recognisedText.blocks.expand((block) => block.lines).toList();

    expandedLines.sort((a, b) => a.rect.top.compareTo(b.rect.top));

    while (expandedLines.length > 0) {
      final lines = <TextLine>[];
      final firstLine = expandedLines[0];

      lines.add(firstLine);

      for (int i = 1; i < expandedLines.length; i++) {
        final otherLine = expandedLines[i];
        final dt = (otherLine.rect.center.dy - firstLine.rect.center.dy).abs();
        final halfHeight = firstLine.rect.height / ratio;
        if (dt <= halfHeight && !firstLine.rect.overlaps(otherLine.rect)) {
          lines.add(otherLine);
        }
      }

      lines.sort((a, b) => a.rect.left.compareTo(b.rect.left));

      blocks.add(
          TextBlock._compact(lines.map((e) => e.text).join(" ").trim(), lines));

      expandedLines.removeWhere((e) => lines.contains(e));
    }

    return blocks;
  }
}

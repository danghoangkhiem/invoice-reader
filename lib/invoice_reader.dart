library invoice_reader;

import 'dart:async';
import 'dart:collection';
import 'dart:convert' as convert;
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';

export 'src/parsers/aeon_invoice_parser.dart';
export 'src/parsers/bigc_invoice_parser.dart';
export 'src/parsers/coopmart_invoice_parser.dart';
export 'src/parsers/coopxtra_invoice_parser.dart';
export 'src/parsers/emart_invoce_parser.dart';
export 'src/parsers/fuji_invoice_parser.dart';
export 'src/parsers/intimex_invoice_parser.dart';
export 'src/parsers/lotte_invoice_parser.dart';
export 'src/parsers/mega_invoice_parser.dart';
export 'src/parsers/satra_invoice_parser.dart';
export 'src/parsers/vinmart_invoice_parser.dart';
export 'src/parsers/go_invoice_parser.dart';

part 'src/invoice.dart';
part 'src/invoice_dictionary.dart';
part 'src/invoice_dictionary_manager.dart';
part 'src/invoice_parser.dart';
part 'src/invoice_reader.dart';
part 'src/handlers/document_detector.dart';
part 'src/handlers/path_provider.dart';
part 'src/handlers/text_detector.dart';

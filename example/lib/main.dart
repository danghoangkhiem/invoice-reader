import 'dart:typed_data';

import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:invoice_reader/invoice_reader.dart';

void main() {
  runApp(MaterialApp(home: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Uint8List? _imageBytes;

  Invoice? _invoice;

  bool _isEnhancePicture = false;
  bool _isSyncing = false;

  InvoiceType _selectedInvoiceType = InvoiceType.Aeon;

  final _parsers = <InvoiceType, InvoiceParser>{
    InvoiceType.Aeon: AeonInvoiceParser(),
    InvoiceType.BigC: BigcInvoiceParser(),
    InvoiceType.Coopmart: CoopmartInvoiceParser(),
    InvoiceType.CoopXtra: CoopxtraInvoiceParser(),
    InvoiceType.Emart: EmartInvoiceParser(),
    InvoiceType.Fuji: FujiInvoiceParser(),
    InvoiceType.Intimex: IntimexInvoiceParser(),
    InvoiceType.Lotte: LotteInvoiceParser(),
    InvoiceType.Mega: MegaInvoiceParser(),
    InvoiceType.Satra: SatraInvoiceParser(),
    InvoiceType.Vinmart: VinmartInvoiceParser(),
    InvoiceType.Go: GoInvoiceParser(),
  };

  VoidCallback selectPicture(ImageSource source) {
    return () async {
      final picked = await ImagePicker().pickImage(source: source);
      if (picked != null) {
        await process(await picked.readAsBytes());
      }
    };
  }

  Future<void> process(Uint8List bytes) async {
    setState(() {
      _imageBytes = null;
      _invoice = null;
    });

    Uint8List imageBytes = bytes;

    final detectedResult =
        await InvoiceReader.documentDetector.detectDocument(bytes);
    if (detectedResult.points.isNotEmpty) {
      imageBytes = detectedResult.document;
    }

    print('-------------------------------------------------');
    print(' Size: ${detectedResult.size}');
    print(' Points: ${detectedResult.points}');
    print('-------------------------------------------------');

    if (_isEnhancePicture) {
      imageBytes = await InvoiceReader.documentDetector
          .enhancePicture(detectedResult.document);
    }

    setState(() {
      _imageBytes = imageBytes;
    });

    final invoiceResult = await InvoiceReader.process(
      bytes,
      parser: _parsers[_selectedInvoiceType]!,
      enhance: _isEnhancePicture,
    );

    print('--------------- Results ----------------');
    print('Invoice Type: ${invoiceResult.type}');
    print('Invoice Date: ${invoiceResult.date}');
    print('Invoice Number: ${invoiceResult.number}');

    invoiceResult.items.forEach((element) {
      print(element.toMap());
    });

    setState(() {
      _invoice = invoiceResult;
    });
  }

  void syncDictionary() async {
    setState(() {
      _isSyncing = true;
    });
    try {
      await InvoiceDictionaryManager.instance.synchronize();
    } catch (error, stack) {
      print(error);
      print(stack);
    }
    if (mounted) {
      setState(() {
        _isSyncing = false;
      });
    }
  }

  void showResults() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => ResultPage(
          invoice: _invoice!,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Invoice Reader'),
        actions: [
          _isSyncing
              ? Container(
                  width: 70,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    ),
                  ),
                )
              : ElevatedButton(
                  onPressed: syncDictionary,
                  child: Text('Sync'),
                ),
          _invoice != null
              ? ElevatedButton(onPressed: showResults, child: Text('Result'))
              : SizedBox(),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Invoice Type',
                  style: TextStyle(fontSize: 16),
                ),
                Container(
                  width: 150,
                  child: StatefulBuilder(
                    builder: (context, setState) {
                      return DropdownButton<InvoiceType>(
                        isExpanded: true,
                        value: _selectedInvoiceType,
                        onChanged: (value) {
                          if (value != null) {
                            _selectedInvoiceType = value;
                            setState(() {});
                          }
                        },
                        items: List.from(InvoiceType.values.map(
                          (e) => DropdownMenuItem<InvoiceType>(
                            child: Text(
                              e.toString().replaceFirst('InvoiceType.', ''),
                            ),
                            value: e,
                          ),
                        )),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          CheckboxListTile(
            title: Text('Enhance Picture'),
            value: _isEnhancePicture,
            onChanged: (bool? value) {
              _isEnhancePicture = value ?? false;
              setState(() {});
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                onPressed: selectPicture(ImageSource.gallery),
                child: Text("GALLERY"),
              ),
              TextButton(
                onPressed: selectPicture(ImageSource.camera),
                child: Text("CAMERA"),
              ),
            ],
          ),
          Expanded(
            child: _imageBytes != null
                ? ExtendedImage.memory(
                    _imageBytes!,
                    mode: ExtendedImageMode.gesture,
                  )
                : Text("No images"),
          ),
        ],
      ),
    );
  }
}

class ResultPage extends StatelessWidget {
  final Invoice invoice;

  ResultPage({Key? key, required this.invoice}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scanned Results"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            color: Colors.grey,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Invoice Type: ${invoice.type.toString().replaceFirst('InvoiceType.', '')}',
                ),
                Text(
                  'Invoice Number: ${invoice.number ?? 'N/A'}',
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              children: invoice.items.map((e) {
                return Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black54),
                  ),
                  margin: const EdgeInsets.only(bottom: 10),
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('Mã sản phẩm: '),
                          Text(e.code ?? "N/A"),
                        ],
                      ),
                      Row(
                        children: [
                          Text('Tên sản phẩm: '),
                          Text(e.name ?? "N/A"),
                        ],
                      ),
                      Row(
                        children: [
                          Text('VAT: '),
                          e.tax != null ? Text('${e.tax}') : Text('N/A'),
                        ],
                      ),
                      Row(
                        children: [
                          Text('Số lượng: '),
                          e.quantity != null
                              ? Text('${e.quantity}')
                              : Text('N/A')
                        ],
                      ),
                      Row(
                        children: [
                          Text('Khối lượng: '),
                          e.weight != null ? Text('${e.weight}') : Text('N/A')
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: e.price != null
                                ? Text('Giá: ${e.price!.toStringAsFixed(2)}')
                                : SizedBox(),
                          ),
                          Expanded(
                            child: e.totalPrice != null
                                ? Text(
                                    'Tổng giá: ${e.totalPrice!.toStringAsFixed(2)}')
                                : SizedBox(),
                          ),
                          Expanded(
                            child: e.originalPrice != null
                                ? Text(
                                    'Giá gốc: ${e.originalPrice!.toStringAsFixed(2)}')
                                : SizedBox(),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}

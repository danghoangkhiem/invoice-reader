package vn.imark.invoice_reader;

import static org.opencv.core.Core.bitwise_not;

import android.graphics.Bitmap;
import android.graphics.PointF;

import androidx.annotation.NonNull;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class DocumentDetector extends ApiHandler {
  private static final String DETECT_DOCUMENT = "documentDetector#detectDocument";
  private static final String ENHANCE_PICTURE = "documentDetector#enhancePicture";

  private static boolean OpenCVFLag = false;

  private final int maxHeight;

  public DocumentDetector(int maxHeight) {
    this.maxHeight = maxHeight;
  }

  @Override
  public List<String> getMethodsKeys() {
    return Arrays.asList(DETECT_DOCUMENT, ENHANCE_PICTURE);
  }

  @Override
  public void onDetach(@NonNull FlutterPlugin.FlutterPluginBinding binding) {}

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
    if (!OpenCVFLag) {
      if (!OpenCVLoader.initDebug()) {
        System.out.println("Error on load OpenCV");
      } else {
        OpenCVFLag = true;
      }
    }

    switch (call.method) {
      case DETECT_DOCUMENT:
        executeInBackground(() -> handleDetectDocument(call), result);
        break;
      case ENHANCE_PICTURE:
        executeInBackground(() -> handleEnhancePicture(call), result);
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  private static class DocumentDetectorResult {
    final List<PointF> points;
    final Size size;
    final byte[] document;

    public DocumentDetectorResult(List<PointF> points, Size size, byte[] document) {
      this.points = points;
      this.size = size;
      this.document = document;
    }

    public Map<String, Object> toMap() {
      Map<String, Object> map = new HashMap<>();
      List<Map<String, Object>> points = new ArrayList<>();
      for (PointF point : this.points) {
        Map<String, Object> hm = new HashMap<>();
        hm.put("x", point.x);
        hm.put("y", point.y);
        points.add(hm);
      }
      Map<String, Object> size = new HashMap<>();
      size.put("width", this.size.width);
      size.put("height", this.size.height);
      map.put("points", points);
      map.put("size", size);
      map.put("document", document);
      return map;
    }
  }

  private Map<String, Object> handleDetectDocument(MethodCall call) {
    byte[] bytes = call.argument("byteData");

    // Imgcodecs.IMREAD_UNCHANGED
    Mat picture = Imgcodecs.imdecode(new MatOfByte(bytes), Imgcodecs.IMREAD_COLOR);

    List<PointF> points = findPoints(picture);

    Mat transformed;
    if (points.isEmpty()) {
      transformed = picture.clone();
    } else {
      transformed = perspectiveTransform(picture, points);
    }

    MatOfByte matOfByte = new MatOfByte();
    Imgcodecs.imencode(".jpg", transformed, matOfByte);

    Size size = transformed.size();

    picture.release();
    transformed.release();

    byte[] byteArray = matOfByte.toArray();

    matOfByte.release();

    DocumentDetectorResult result = new DocumentDetectorResult(points, size, byteArray);

    return result.toMap();
  }

  private byte[] handleEnhancePicture(MethodCall call) {
    byte[] bytes = call.argument("byteData");

    // Imgcodecs.IMREAD_UNCHANGED
    Mat picture = Imgcodecs.imdecode(new MatOfByte(bytes), Imgcodecs.IMREAD_COLOR);

    Mat enhancedPicture = applyThreshold(picture);

    MatOfByte matOfByte = new MatOfByte();
    Imgcodecs.imencode(".jpg", enhancedPicture, matOfByte);

    picture.release();
    enhancedPicture.release();

    return matOfByte.toArray();
  }

  /**
   * Resize a given bitmap to scale using the given height
   *
   * @return The resized bitmap
   */
  private Bitmap getResizedBitmap(Bitmap bitmap) {
    double ratio = bitmap.getHeight() / (double) maxHeight;
    int width = (int) (bitmap.getWidth() / ratio);
    return Bitmap.createScaledBitmap(bitmap, width, maxHeight, false);
  }

  /**
   * Attempt to find the four corner points for the largest contour in the image.
   *
   * @return A list of points, or null if a valid rectangle cannot be found.
   */
  private List<PointF> findPoints(Mat src) {
    List<PointF> result = new ArrayList<>();

    Mat image = new Mat();

    Bitmap bm = Bitmap.createBitmap(src.width(), src.height(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(src, bm);

    Utils.bitmapToMat(getResizedBitmap(bm), image);

    Mat edges = edgeDetection(image);
    MatOfPoint2f largest = findLargestContour(edges);

    if (largest != null) {
      Point[] points = sortPoints(largest.toArray());
      result = new ArrayList<>();
      result.add(
          new PointF(
              Double.valueOf(points[0].x).floatValue(), Double.valueOf(points[0].y).floatValue()));
      result.add(
          new PointF(
              Double.valueOf(points[1].x).floatValue(), Double.valueOf(points[1].y).floatValue()));
      result.add(
          new PointF(
              Double.valueOf(points[2].x).floatValue(), Double.valueOf(points[2].y).floatValue()));
      result.add(
          new PointF(
              Double.valueOf(points[3].x).floatValue(), Double.valueOf(points[3].y).floatValue()));
      largest.release();
    } else {
      System.out.println("Can't find rectangle!");
    }

    edges.release();
    image.release();

    return result;
  }

  /**
   * Detect the edges in the given Mat
   *
   * @param src A valid Mat object
   * @return A Mat processed to find edges
   */
  private Mat edgeDetection(Mat src) {
    Mat edges = new Mat();
    Imgproc.cvtColor(src, edges, Imgproc.COLOR_BGR2GRAY);
    Imgproc.GaussianBlur(edges, edges, new Size(5, 5), 0);
    Imgproc.Canny(edges, edges, 75, 200);
    return edges;
  }

  /**
   * Find the largest 4 point contour in the given Mat.
   *
   * @param src A valid Mat
   * @return The largest contour as a Mat
   */
  private MatOfPoint2f findLargestContour(Mat src) {
    List<MatOfPoint> contours = new ArrayList<>();
    Imgproc.findContours(src, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

    // Get the 5 largest contours
    Collections.sort(
        contours,
        (o1, o2) -> {
          double area1 = Imgproc.contourArea(o1);
          double area2 = Imgproc.contourArea(o2);
          return (int) (area2 - area1);
        });
    if (contours.size() > 5) contours.subList(4, contours.size() - 1).clear();

    MatOfPoint2f largest = null;
    for (MatOfPoint contour : contours) {
      MatOfPoint2f approx = new MatOfPoint2f();
      MatOfPoint2f c = new MatOfPoint2f();
      contour.convertTo(c, CvType.CV_32FC2);
      Imgproc.approxPolyDP(c, approx, Imgproc.arcLength(c, true) * 0.02, true);

      if (approx.total() == 4 && Imgproc.contourArea(contour) > 150) {
        // the contour has 4 points, it's valid
        largest = approx;
        break;
      }
    }

    return largest;
  }

  /**
   * Transform the coordinates on the given Mat to correct the perspective.
   *
   * @param src A valid Mat
   * @param points A list of coordinates from the given Mat to adjust the perspective
   * @return A perspective transformed Mat
   */
  private Mat perspectiveTransform(Mat src, List<PointF> points) {
    Point point1 = new Point(points.get(0).x, points.get(0).y);
    Point point2 = new Point(points.get(1).x, points.get(1).y);
    Point point3 = new Point(points.get(2).x, points.get(2).y);
    Point point4 = new Point(points.get(3).x, points.get(3).y);
    Point[] pts = {point1, point2, point3, point4};
    return fourPointTransform(src, sortPoints(pts));
  }

  /**
   * Apply a threshold to give the "scanned" look
   *
   * <p>NOTE: See the following link for more info
   * http://docs.opencv.org/3.1.0/d7/d4d/tutorial_py_thresholding.html#gsc.tab=0
   *
   * @param src A valid Mat
   * @return The processed Mat
   */
  private Mat applyThreshold(Mat src) {
    Mat dst = new Mat();

    Imgproc.cvtColor(src, dst, Imgproc.COLOR_BGR2GRAY);

    // Some other approaches
    Imgproc.adaptiveThreshold(
        dst, dst, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 15, 15);
    Imgproc.threshold(dst, dst, 0, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);

    //    Imgproc.GaussianBlur(dst, dst, new Size(5, 5), 0);
    //    Imgproc.adaptiveThreshold(
    //        dst, dst, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 11, 2);

    return dst;
  }

  /**
   * Sort the points
   *
   * <p>The order of the points after sorting: 0------->1 ^ | | v 3<-------2
   *
   * <p>NOTE: Based off of
   * http://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/
   *
   * @param src The points to sort
   * @return An array of sorted points
   */
  private Point[] sortPoints(Point[] src) {
    ArrayList<Point> srcPoints = new ArrayList<>(Arrays.asList(src));
    Point[] result = {null, null, null, null};

    Comparator<Point> sumComparator =
        (lhs, rhs) -> Double.valueOf(lhs.y + lhs.x).compareTo(rhs.y + rhs.x);
    Comparator<Point> differenceComparator =
        (lhs, rhs) -> Double.valueOf(lhs.y - lhs.x).compareTo(rhs.y - rhs.x);

    result[0] = Collections.min(srcPoints, sumComparator); // Upper left has the minimal sum
    result[2] = Collections.max(srcPoints, sumComparator); // Lower right has the maximal sum
    result[1] =
        Collections.min(srcPoints, differenceComparator); // Upper right has the minimal difference
    result[3] =
        Collections.max(srcPoints, differenceComparator); // Lower left has the maximal difference

    return result;
  }

  /**
   * NOTE: Based off of
   * http://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/
   *
   * @param src A valid Mat
   * @param pts A list of coordinates from the given Mat
   * @return The processed Mat
   */
  private Mat fourPointTransform(Mat src, Point[] pts) {
    double ratio = src.size().height / (double) maxHeight;

    Point ul = pts[0];
    Point ur = pts[1];
    Point lr = pts[2];
    Point ll = pts[3];

    double widthA = Math.sqrt(Math.pow(lr.x - ll.x, 2) + Math.pow(lr.y - ll.y, 2));
    double widthB = Math.sqrt(Math.pow(ur.x - ul.x, 2) + Math.pow(ur.y - ul.y, 2));
    double maxWidth = Math.max(widthA, widthB) * ratio;

    double heightA = Math.sqrt(Math.pow(ur.x - lr.x, 2) + Math.pow(ur.y - lr.y, 2));
    double heightB = Math.sqrt(Math.pow(ul.x - ll.x, 2) + Math.pow(ul.y - ll.y, 2));
    double maxHeight = Math.max(heightA, heightB) * ratio;

    Mat resultMat =
        new Mat(
            Double.valueOf(maxHeight).intValue(),
            Double.valueOf(maxWidth).intValue(),
            CvType.CV_8UC4);

    Mat srcMat = new Mat(4, 1, CvType.CV_32FC2);
    Mat dstMat = new Mat(4, 1, CvType.CV_32FC2);
    srcMat.put(
        0,
        0,
        ul.x * ratio,
        ul.y * ratio,
        ur.x * ratio,
        ur.y * ratio,
        lr.x * ratio,
        lr.y * ratio,
        ll.x * ratio,
        ll.y * ratio);
    dstMat.put(0, 0, 0.0, 0.0, maxWidth, 0.0, maxWidth, maxHeight, 0.0, maxHeight);

    Mat M = Imgproc.getPerspectiveTransform(srcMat, dstMat);
    Imgproc.warpPerspective(src, resultMat, M, resultMat.size());

    srcMat.release();
    dstMat.release();
    M.release();

    return resultMat;
  }

  /**
   * NOTE: Based off of https://github.com/JPLeoRX/opencv-text-deskew
   *
   * @param src A valid Mat
   * @return The processed Mat
   */
  private Mat deskew(Mat src) {
    Mat img = src.clone();
    Mat dst = new Mat();

    // Prep image, copy, convert to gray scale, blur, and threshold
    Imgproc.cvtColor(img, dst, Imgproc.COLOR_BGR2GRAY);
    Imgproc.GaussianBlur(dst, dst, new Size(5, 5), 0);
    bitwise_not(dst, dst);
    Imgproc.threshold(dst, dst, 0, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);

    // Apply dilate to merge text into meaningful lines/paragraphs.
    // Use larger kernel on X axis to merge characters into single line, cancelling out any spaces.
    // But use smaller kernel on Y axis to separate between different blocks of text
    Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(30, 5));
    Imgproc.dilate(dst, dst, kernel, new Point(-1, -1), 5);

    // Find all contours
    List<MatOfPoint> contours = new ArrayList<>();
    Imgproc.findContours(dst, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

    Collections.sort(
        contours,
        (o1, o2) -> {
          double area1 = Imgproc.contourArea(o1);
          double area2 = Imgproc.contourArea(o2);
          return (int) (area2 - area1);
        });

    // Find largest contour and surround in min area box
    MatOfPoint largestContour = contours.get(0);
    RotatedRect minAreaRect = Imgproc.minAreaRect(new MatOfPoint2f(largestContour.toArray()));

    // Determine the angle. Convert it to the value that was originally used to obtain skewed image
    double angle = minAreaRect.angle;
    if (angle < -45) {
      angle = 90 + angle;
    }
    angle = -1.0 * angle;

    // Rotate the image by the residual offset
    Point center = new Point((double) img.width() / 2, (double) img.height() / 2);
    Size size = new Size(img.width(), img.height());
    Mat M = Imgproc.getRotationMatrix2D(center, -1.0 * angle, 1.0);
    Imgproc.warpAffine(img, img, M, size, Imgproc.INTER_CUBIC);

    dst.release();

    return img;
  }
}

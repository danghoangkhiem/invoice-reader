package vn.imark.invoice_reader;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.SettableFuture;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

abstract class ApiHandler {
  private final Executor uiThreadExecutor = new UiThreadExecutor();
  private final Executor executor =
      Executors.newSingleThreadExecutor(
          new ThreadFactoryBuilder()
              .setNameFormat("invoice-reader-background-%d")
              .setPriority(Thread.NORM_PRIORITY)
              .build());

  abstract List<String> getMethodsKeys();

  abstract void onDetach(@NonNull FlutterPlugin.FlutterPluginBinding binding);

  abstract void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result);

  <T> void executeInBackground(Callable<T> task, MethodChannel.Result result) {
    final SettableFuture<T> future = SettableFuture.create();
    Futures.addCallback(
        future,
        new FutureCallback<T>() {
          public void onSuccess(T answer) {
            result.success(answer);
          }

          public void onFailure(@NonNull Throwable t) {
            result.error(t.getClass().getName(), t.getMessage(), null);
          }
        },
        uiThreadExecutor);
    executor.execute(
        () -> {
          try {
            future.set(task.call());
          } catch (Throwable t) {
            future.setException(t);
          }
        });
  }

  private static class UiThreadExecutor implements Executor {
    private final Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public void execute(Runnable command) {
      handler.post(command);
    }
  }
}

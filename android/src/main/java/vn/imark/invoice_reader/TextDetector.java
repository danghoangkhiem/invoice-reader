package vn.imark.invoice_reader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.nfc.FormatException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

// Detector to identify the text  present in an image.
// It's an abstraction over TextRecognition provided by ml tool kit.
public class TextDetector extends ApiHandler {
  private static final String START = "textDetector#startDetection";
  private static final String CLOSE = "textDetector#closeDetection";

  private TextRecognizer textRecognizer;

  @Override
  public List<String> getMethodsKeys() {
    return Arrays.asList(START, CLOSE);
  }

  @Override
  public void onDetach(@NonNull FlutterPlugin.FlutterPluginBinding binding) {
    if (this.textRecognizer != null) {
      this.closeDetector();
      this.textRecognizer = null;
    }
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
    String method = call.method;
    if (method.equals(START)) {
      handleDetection(call, result);
    } else if (method.equals(CLOSE)) {
      closeDetector();
      result.success(null);
    } else {
      result.notImplemented();
    }
  }

  private void handleDetection(MethodCall call, final MethodChannel.Result result) {
    Map<String, Object> imageData = call.argument("imageData");

    if (imageData == null) {
      result.error("InputImageConverterError", "Invalid Input Image", null);
      return;
    }

    InputImage inputImage;

    try {
      inputImage = getInputImageFromData(imageData);
    } catch (Exception e) {
      Log.e("ImageError", "Getting Image failed");
      e.printStackTrace();
      result.error("InputImageConverterError", e.toString(), null);
      return;
    }

    if (textRecognizer == null)
      textRecognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);

    textRecognizer
        .process(inputImage)
        .addOnSuccessListener(
            text -> {
              Map<String, Object> textResult = new HashMap<>();

              textResult.put("text", text.getText());

              List<Map<String, Object>> textBlocks = new ArrayList<>();
              for (Text.TextBlock block : text.getTextBlocks()) {
                Map<String, Object> blockData = new HashMap<>();

                addData(
                    blockData,
                    block.getText(),
                    block.getBoundingBox(),
                    block.getCornerPoints(),
                    block.getRecognizedLanguage());

                List<Map<String, Object>> textLines = new ArrayList<>();
                for (Text.Line line : block.getLines()) {
                  Map<String, Object> lineData = new HashMap<>();

                  addData(
                      lineData,
                      line.getText(),
                      line.getBoundingBox(),
                      line.getCornerPoints(),
                      line.getRecognizedLanguage());

                  List<Map<String, Object>> elementsData = new ArrayList<>();
                  for (Text.Element element : line.getElements()) {
                    Map<String, Object> elementData = new HashMap<>();

                    addData(
                        elementData,
                        element.getText(),
                        element.getBoundingBox(),
                        element.getCornerPoints(),
                        element.getRecognizedLanguage());

                    elementsData.add(elementData);
                  }
                  lineData.put("elements", elementsData);
                  textLines.add(lineData);
                }
                blockData.put("lines", textLines);
                textBlocks.add(blockData);
              }
              textResult.put("blocks", textBlocks);
              result.success(textResult);
            })
        .addOnFailureListener(e -> result.error("TextDetectorError", e.toString(), null));
  }

  private void addData(
      Map<String, Object> addTo,
      String text,
      Rect rect,
      Point[] cornerPoints,
      String recognizedLanguage) {
    List<String> recognizedLanguages = new ArrayList<>();
    recognizedLanguages.add(recognizedLanguage);
    List<Map<String, Integer>> points = new ArrayList<>();
    addPoints(cornerPoints, points);
    addTo.put("points", points);
    addTo.put("rect", getBoundingPoints(rect));
    addTo.put("recognizedLanguages", recognizedLanguages);
    addTo.put("text", text);
  }

  private void addPoints(Point[] cornerPoints, List<Map<String, Integer>> points) {
    for (Point point : cornerPoints) {
      Map<String, Integer> p = new HashMap<>();
      p.put("x", point.x);
      p.put("y", point.y);
      points.add(p);
    }
  }

  private Map<String, Integer> getBoundingPoints(Rect rect) {
    Map<String, Integer> frame = new HashMap<>();
    frame.put("left", rect.left);
    frame.put("right", rect.right);
    frame.put("top", rect.top);
    frame.put("bottom", rect.bottom);
    return frame;
  }

  private void closeDetector() {
    textRecognizer.close();
    textRecognizer = null;
  }

  // Returns an [InputImage] from the image data received
  static InputImage getInputImageFromData(Map<String, Object> imageData) throws FormatException {
    byte[] bytes = (byte[]) imageData.get("byteData");
    if (bytes == null) {
      bytes = new byte[0];
    }

    int rotation;
    Object r = imageData.get("rotation");
    if (r == null) {
      rotation = 0;
    } else {
      rotation = (int) r;
    }

    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

    if (bitmap == null) {
      throw new FormatException("The image could not be decoded");
    }

    return InputImage.fromBitmap(bitmap, rotation);
  }
}

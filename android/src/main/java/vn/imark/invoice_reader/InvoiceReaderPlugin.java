package vn.imark.invoice_reader;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** InvoiceReaderPlugin */
public class InvoiceReaderPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;

  private Map<String, ApiHandler> methodHandlers;
  private List<ApiHandler> apiHandlers;

  private void initHandlers(Context context) {
    apiHandlers =
        Arrays.asList(new DocumentDetector(500), new TextDetector(), new PathProvider(context));

    methodHandlers = new HashMap<>();
    for (ApiHandler handler : apiHandlers) {
      for (String method : handler.getMethodsKeys()) {
        methodHandlers.put(method, handler);
      }
    }
  }

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    initHandlers(flutterPluginBinding.getApplicationContext());
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "invoice_reader");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    ApiHandler handler = methodHandlers.get(call.method);
    if (handler != null) {
      handler.onMethodCall(call, result);
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    for (ApiHandler handler : apiHandlers) {
      handler.onDetach(binding);
    }
    channel.setMethodCallHandler(null);
    channel = null;
  }
}

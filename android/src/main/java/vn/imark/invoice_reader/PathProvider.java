package vn.imark.invoice_reader;

import android.content.Context;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.util.PathUtils;

public class PathProvider extends ApiHandler {
  private static final String TEMPORARY_DIRECTORY = "pathProvider#getTemporaryDirectory";
  private static final String APPLICATION_DOCUMENTS_DIRECTORY =
      "pathProvider#getApplicationDocumentsDirectory";
  private static final String STORAGE_DIRECTORY = "pathProvider#getStorageDirectory";
  private static final String EXTERNAL_CACHE_DIRECTORIES =
      "pathProvider#getExternalCacheDirectories";
  private static final String EXTERNAL_STORAGE_DIRECTORIES =
      "pathProvider#getExternalStorageDirectories";
  private static final String APPLICATION_SUPPORT_DIRECTORY =
      "pathProvider#getApplicationSupportDirectory";

  private final Context context;

  public PathProvider(Context context) {
    this.context = context;
  }

  @Override
  List<String> getMethodsKeys() {
    return Arrays.asList(
        TEMPORARY_DIRECTORY,
        APPLICATION_DOCUMENTS_DIRECTORY,
        STORAGE_DIRECTORY,
        EXTERNAL_CACHE_DIRECTORIES,
        EXTERNAL_STORAGE_DIRECTORIES,
        APPLICATION_SUPPORT_DIRECTORY);
  }

  @Override
  void onDetach(@NonNull FlutterPlugin.FlutterPluginBinding binding) {}

  @Override
  void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
    switch (call.method) {
      case TEMPORARY_DIRECTORY:
        executeInBackground(() -> getPathProviderTemporaryDirectory(), result);
        break;
      case APPLICATION_DOCUMENTS_DIRECTORY:
        executeInBackground(() -> getPathProviderApplicationDocumentsDirectory(), result);
        break;
      case STORAGE_DIRECTORY:
        executeInBackground(() -> getPathProviderStorageDirectory(), result);
        break;
      case EXTERNAL_CACHE_DIRECTORIES:
        executeInBackground(() -> getPathProviderExternalCacheDirectories(), result);
        break;
      case EXTERNAL_STORAGE_DIRECTORIES:
        final Integer type = call.argument("type");
        final String directoryName = StorageDirectoryMapper.androidType(type);
        executeInBackground(() -> getPathProviderExternalStorageDirectories(directoryName), result);
        break;
      case APPLICATION_SUPPORT_DIRECTORY:
        executeInBackground(() -> getApplicationSupportDirectory(), result);
        break;
      default:
        result.notImplemented();
    }
  }

  private String getPathProviderTemporaryDirectory() {
    return context.getCacheDir().getPath();
  }

  private String getApplicationSupportDirectory() {
    return PathUtils.getFilesDir(context);
  }

  private String getPathProviderApplicationDocumentsDirectory() {
    return PathUtils.getDataDirectory(context);
  }

  private String getPathProviderStorageDirectory() {
    final File dir = context.getExternalFilesDir(null);
    if (dir == null) {
      return null;
    }
    return dir.getAbsolutePath();
  }

  private List<String> getPathProviderExternalCacheDirectories() {
    final List<String> paths = new ArrayList<>();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      for (File dir : context.getExternalCacheDirs()) {
        if (dir != null) {
          paths.add(dir.getAbsolutePath());
        }
      }
    } else {
      File dir = context.getExternalCacheDir();
      if (dir != null) {
        paths.add(dir.getAbsolutePath());
      }
    }

    return paths;
  }

  private List<String> getPathProviderExternalStorageDirectories(String type) {
    final List<String> paths = new ArrayList<>();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      for (File dir : context.getExternalFilesDirs(type)) {
        if (dir != null) {
          paths.add(dir.getAbsolutePath());
        }
      }
    } else {
      File dir = context.getExternalFilesDir(type);
      if (dir != null) {
        paths.add(dir.getAbsolutePath());
      }
    }

    return paths;
  }

  /** Helps to map the Dart `StorageDirectory` enum to a Android system constant. */
  private static class StorageDirectoryMapper {
    /**
     * Return a Android Environment constant for a Dart Index.
     *
     * @return The correct Android Environment constant or null, if the index is null.
     * @throws IllegalArgumentException If `dartIndex` is not null but also not matches any known
     *     index.
     */
    static String androidType(Integer dartIndex) throws IllegalArgumentException {
      if (dartIndex == null) {
        return null;
      }

      switch (dartIndex) {
        case 0:
          return Environment.DIRECTORY_MUSIC;
        case 1:
          return Environment.DIRECTORY_PODCASTS;
        case 2:
          return Environment.DIRECTORY_RINGTONES;
        case 3:
          return Environment.DIRECTORY_ALARMS;
        case 4:
          return Environment.DIRECTORY_NOTIFICATIONS;
        case 5:
          return Environment.DIRECTORY_PICTURES;
        case 6:
          return Environment.DIRECTORY_MOVIES;
        case 7:
          return Environment.DIRECTORY_DOWNLOADS;
        case 8:
          return Environment.DIRECTORY_DCIM;
        case 9:
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Environment.DIRECTORY_DOCUMENTS;
          } else {
            throw new IllegalArgumentException("Documents directory is unsupported.");
          }
        default:
          throw new IllegalArgumentException("Unknown index: " + dartIndex);
      }
    }
  }
}
